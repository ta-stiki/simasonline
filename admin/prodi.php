<?php ob_start();?>
<?php session_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['level'] == "Admin"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!-- Bootstrap core CSS -->
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				<link href="../assets/css/custom.css" rel="stylesheet">
				<!-- Datatabel Plugin -->
				<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
				<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
				<script src="../datatabel/js/jquery.dataTables.js"></script>
				<script src="../datatabel/js/fixedtabel.min.js"></script>
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Master Data Prodi</b></h4>
								</header>
							</div>
						</center>
						</br>
						<form method="POST" action="">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button name="btnNew" class="btn btn-primary" type="submit">Tambah Data Prodi</button>
						</form>
														
						<!-- Modal Tamabh Staf -->
						<?php if (isset($_POST['btnNew'])) { ?>
						<div class="modal fade bs-example-modal-lg" id="addModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
								<form method="POST" action="">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Tambah Data Prodi</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Prodi</label>
															<div class="col-sm-10">
																<input name="prodi" id="prodi" placeholder="Prodi" class="form-control" type="text" required>
															</div>
														</div>
														<!-- Text input-->							
													</fieldset>
													<div class="modal-footer">
														<button name="btnAdd" class="btn btn-primary" type="submit">Simpan</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>												
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
								</form>
								</div>
							</div>
						</div>
						<?php } ?>
						<!-- And Modal Tamabah Staf -->
						
						<div id="batas" class="module_content">
							
								<div class="table-responsive">
									<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
										<thead>
											<tr bgcolor="#F5F5F5">
												<th>No </th>
												<th>Prodi &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
										include("../assets/koneksi/koneksi.php");									
										?>
											<?php
											$view=mysql_query("select * from tbl_prodi");
											$no=0;
											while($row=mysql_fetch_array($view)){
												$no++;
											?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $row['prodi'];?></td>
													<td><?php echo $row['status'];?></td>
													<td> 
														<form method="POST" action="">
															<input type="hidden" name="id_prodi" value="<?php echo $row['id_prodi']; ?>">
															<button name="btnEdit" class="btn btn-success" type="submit">Ubah</button>
														</form>
													</td>
												</tr>
											<?php
											}
											?>
										</tbody>
										<!-- JavaScript Untuk datatabel scroll-->
										<script>
											$(document).ready(function() {
												$('#example').DataTable( {
													"scrollY": 200,
													"scrollX": true
												} );
											} );
										</script>
										<!-- end JavaScript Untuk datatabel scroll-->
									</table>
								</div>
						</div>
					</article>
				</div>
								
				<!-- Modal Edit Staf -->
				<?php if (isset($_POST['btnEdit'])) { ?>
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Prodi</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_prodi WHERE id_prodi='$_POST[id_prodi]' ");
										  while ($show = mysql_fetch_array($shows)) { 
											?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Id Prodi</label>
															<div class="col-sm-10">
																<input type="hidden" name="id_prodi" value="<?php echo $show['id_prodi']; ?>">
																<input name="id_prodi" id="id_prodi" placeholder="Id Prodi" class="form-control" type="text" value="<?php echo $show['id_prodi']; ?>" required disabled>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Prodi</label>
															<div class="col-sm-10">
																<input name="prodi" id="prodi" placeholder="Prodi" class="form-control" type="text" value="<?php echo $show['prodi']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Status</label>
																<div class="col-sm-4">
																	<select name="status" required>
																		<option></option>
																		<?php
																		if ($show['status'] == 'Aktif') {
																			echo "<option value='Aktif' selected>Aktif</option>";
																			echo "<option value='Tidak Aktif'>Tidak Aktif</option>";
																		  }
																		  if ($show['status'] == 'Tidak Aktif') {
																			echo "<option value='Tidak Aktif' selected>Tidak Aktif</option>";
																			echo "<option value='Aktif'>Aktif</option>";
																		  }
																		 ?>
																	  </select>				
																</div>
														</div>
														<!-- Text input-->
													</fieldset>
													<div class="modal-footer">
														<button name="btnUpdate" class="btn btn-primary" type="submit">Simpan</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
													<?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Staf -->
				<?php } ?>				
				
				<!-- PHP Untuk Insert Data Staf -->
				<?php
					if (isset($_POST['btnAdd'])) {
						$prodi=$_POST['prodi'];
						$status='Aktif';
						  $insert_query = mysql_query("insert into tbl_prodi SET prodi='$prodi', status='$status' ")or die();
						  if($insert_query) {
							echo "<script type='text/javascript'>alert('Data Prodi Berhasil Disimpan')</script>";
							echo "<script>setTimeout(\"location.href = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]';\",0);</script>";
						  } 
							else
								{
									echo '<script language="javascript">alert("Data Prodi Gagal Disimpan"); document.location="../admin/prodi.php";</script>';  
								}
					}
				?>
				<!-- And PHP Untuk Insert Data Staf  -->
				
				<!-- PHP Untuk Update Data Staf  -->
				<?php
					if (isset($_POST['btnUpdate'])) {
						  $update_query = mysql_query("UPDATE tbl_prodi SET prodi='$_POST[prodi]', status='$_POST[status]' WHERE id_prodi='$_POST[id_prodi]' ");
						  if($update_query) {
							echo "<script type='text/javascript'>alert('Data Prodi Berhasil Diubah')</script>";
							echo "<script>setTimeout(\"location.href = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]';\",0);</script>";
						  } 
						  else
							{
								echo '<script language="javascript">alert("Data Prodi Gagal Diubah"); document.location="../admin/prodi.php";</script>';  
							}
					}
				?>
				<!-- And PHP Untuk Update Data Staf  -->
								
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				<!--JavaScript Untuk datatabel-->
				<script>
				$(document).ready(function() {
				$('#example').DataTable();
				} );
				</script>
				
				<script type="text/javascript">
				  // SHOW EDIT MODAL ON LOAD
					$(window).load(function(){
						$("#editModal").modal("show");
						$("#addModal").modal("show");
					});
				</script>
				<!-- End JavaScript Untuk datatabel-->
				
			</body>
		</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>