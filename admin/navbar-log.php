<link href="../select2-master/dist/css/select2.min.css" rel="stylesheet">
<script src="../select2-master/dist/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/tinymce/tinymce.min.js"></script>
<script src="../assets/js/bootstrap-datepicker.js"></script>
<link href="../assets/css/datepicker.css" rel="stylesheet">
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
		</button>
	  
    </div>
	
	<!--Modal Logout-->
	<div class="modal bs-example-modal-md fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6 center-block">
							<form action="" method="post" name="insertform">		
								<center><h4>Anda Yakin Untuk Keluar ?</h4></center>								
								<center>
									<div class="login-block-daftar">
										<!--Scrip PHP untuk insert pendaftaran akun-->
					
										<!--End Scrip PHP untuk insert pendaftaran akun-->	
									</div>
								</center>			
							</form>
						</div>	
					</div>
				</div>
				<div class="modal-footer">															
					<a href="logout.php"><button type="button" class="btn btn-primary" >Ya</button></a>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
				</div>
			</div>
		</div>
	</div>
	<!--And Modal Logout-->
	
	<!--NavBar Setelah Login-->
    <div>
      <div class="collapse navbar-collapse" id="myNavbar">
		<center>
			<ul class="nav navbar-nav">
				<li><a href="index.php"><b>Beranda</b></a></li>
				<!--li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rekam Surat <span class="caret"></span></a>
					<ul class="dropdown-menu"-->
						<li><a href="#section2" data-toggle="modal" data-target=".bs-example-modal-lg1"><b>Surat Masuk</b></a></li>
						<!--li><a href="#section3" data-toggle="modal" data-target=".bs-example-modal-lg2">Surat Keluar</a></li-->
					<!--/ul>
				</li-->
				<li><a href="#" data-toggle="modal" data-target=".bs-example-modal-lg3"><b>Tulis Surat</b></a></li>
				<li><a href="surat.php"><b>Data Surat</b></a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Master Data</b><span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="staf.php">Staf</a></li>
						<li><a href="jabatan.php">Jabatan</a></li>
						<li><a href="prodi.php">Prodi</a></li>
						<li><a href="sifat_surat.php">Sifat Surat</a></li>
					</ul>
				</li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Laporan</b><span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="laporan1.php">Surat Masuk</a></li>
						<li><a href="laporan2.php">Surat Keluar</a></li>
                                                <li><a href="laporan_disposisi.php" target="_blank">Disposisi Surat Masuk</a></li>
						<li><a href="laporan_disposisi2.php" target="_blank">Disposisi Surat Keluar</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" data-toggle="modal" data-target="#myModal"><b>Logout</b></a></li>
			</ul>
		</center>
      </div>
    </div>
	<!--NavBar Setelah Login-->
	
	<!-- Modal Surat Masuk -->
	<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form method="POST" action="" enctype="multipart/form-data" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myLargeModalLabel1">Form Surat Masuk</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
								<fieldset>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Nomor Surat</label>
										<div class="col-sm-10">
											<input name="no_surat" id="no_surat" placeholder="Input No Surat" class="form-control" type="text" required >
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Surat Dari</label>
										<div class="col-sm-10">
											<input name="surat_dari" id="dari" placeholder="Surat Dari" class="form-control" type="text" required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput" >Tgl. Surat</label>
										<div class="col-sm-4">
											<input name="tgl_surat" type="text" placeholder="Tanggal Surat" class="form-control" id="date1" name="date1" required>
										</div>
										<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
										<div class="col-sm-4">
											<select name="sifat_surat" required>
												<?php
													include("../assets/koneksi/koneksi.php");
													$query = "select * from tbl_sifat_surat";
													echo "<option value='' selected>--Pilih Sifat Surat--</option>";
													$hasil = mysql_query($query);
													while ($qtabel = mysql_fetch_assoc($hasil))
													{
														echo '<option value="'.$qtabel['id_sifat'].'">'.$qtabel['sifat_surat'].'</option>';				
													}
												?>
											</select>
										</div>
									</div>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Prihal</label>
										<div class="col-sm-10">
											<input name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text" required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Keterangan</label>
										<div class="col-sm-10">
											<textarea name="keterangan" placeholder="Keterangan" class="form-control" ></textarea>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Pilih File</label>
										<div class="col-sm-10">
											<input type="file" name="img" id="img" class="form-control" required>
										</div>
									</div>
									<!-- Text input-->
									<!--Tabs Disposisi Surat-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Disposisi Ke</label>
											<div class="col-sm-10">
												<select id="disposisi" name="disposisi[]" class="js-example-responsive" multiple="multiple" style="width: 100%" required>
													<option class="form-control" ></option>
														<?php
															$tampil=mysql_query("select * from tbl_staf order by id_staf asc");
															while ($data = mysql_fetch_array($tampil))
															{
														?>
															<option class="form-control" value= "<?php echo $data['id_staf']; ?>"> <?php echo $data['nama']; ?>
															</option>
														<?php	
															}
														?>
												</select>	
											</div>
											
											<!--java Scrip untuk select2-->
											<script>
												$("#disposisi").select2({
														placeholder: "Please Select"
													});
											</script>
											<!--End java Scrip untuk select2-->
									</div>
									<!--Tabs Disposisi Surat-->
									
								</fieldset>
								<div class="modal-footer">
									<button name="btnAdd3" class="btn btn-primary" type="submit" >Simpan</button>
									<a href="index.php"><button type="button" class="btn btn-danger" data-dismiss="">Batal</button></a>											
								</div>
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- And Modal Surat Masuk -->
	
	<!-- PHP Simapan Surat Masuk -->
	<?php
		error_reporting(0);
		$ekstensi_diperbolehkan	= array('png','jpg','pdf');
		$folder="../uploads/";
		$img = rand(1000,100000)."-".$_FILES['img']['name'];
		$x = explode('.', $img);
		$ekstensi = strtolower(end($x));
		$img_loc = $_FILES['img']['tmp_name'];
		$ukuran=$_FILES['img']['size'];
		
		if (isset($_POST['btnAdd3'])) {
			$tgl_terima=date('y-m-d');
			
			if ($_FILES['img']['name'] !==''){
				if(in_array($ekstensi, $ekstensi_diperbolehkan) === false){
					echo '<script language="javascript">alert("Ektensi File Tidak Diijinkan !"); document.location="../admin/surat.php";</script>';
				}
				if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
					if($_FILES['img']['size'] <= 100000){
						echo '<script language="javascript">alert("Upload Gagal, Ukuran File Melebihi 1 Megabyte !"); document.location="../admin/surat.php";</script>';  
					}
					else{
						move_uploaded_file($img_loc,$folder.$img);
						$insert_query = mysql_query("INSERT INTO tbl_surat(no_surat,id_sifat,surat_dari,prihal,tgl_surat,tgl_terima,keterangan,jenis_surat,lampiran) VALUES ('$_POST[no_surat]','$_POST[sifat_surat]','$_POST[surat_dari]','$_POST[prihal]','$_POST[tgl_surat]','$tgl_terima','$_POST[keterangan]','Masuk','$img')");
						if($insert_query) {
							if(count($_POST['disposisi'])){
								foreach($_POST['disposisi'] as $row){
									$insert_query = mysql_query(" insert into tbl_disposisi(no_surat,id_staf,status_terbaca) values ('$_POST[no_surat]','$row','Tidak' ) ");
								}
							}				
							echo "<script language='javascript'>alert('Data Surat Berhasil Disimpan'); document.location='../admin/kirim.php?no_surat=$_POST[no_surat]';</script>"; 
							//header ('location:kirim.php?no_surat=$_POST[no_surat]');
						} 
						else{
							echo '<script language="javascript">alert("Data Surat Gagal Disimpan"); document.location="../admin/surat.php";</script>';  
						}
					}
				}
			}			
		}	
	?>
	<!-- End PHP Simapan Surat Masuk -->
		
	<!-- Modal Surat Keluar -->
	<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel2">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form method="POST" action="" enctype="multipart/form-data" >
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myLargeModalLabel2">Form Surat Keluar</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
								<fieldset>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Nomor Surat</label>
										<div class="col-sm-10">
											<input name="no_surat" id="no_surat" placeholder="Input No Surat" class="form-control" type="text" required >
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Surat Dari</label>
										<div class="col-sm-10">
											<input name="surat_dari" id="dari" placeholder="Surat Dari" class="form-control" type="text" required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput" >Tgl. Surat</label>
										<div class="col-sm-4">
											<input name="tgl_surat" type="text" placeholder="Tanggal Surat" class="form-control" id="date2" name="date2" required>
										</div>
										<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
										<div class="col-sm-4">
											<select name="sifat_surat" required>
												<?php
													include("../assets/koneksi/koneksi.php");
													$query = "select * from tbl_sifat_surat";
													echo "<option value='' selected>--Pilih Sifat Surat--</option>";
													$hasil = mysql_query($query);
													while ($qtabel = mysql_fetch_assoc($hasil))
													{
														echo '<option value="'.$qtabel['id_sifat'].'">'.$qtabel['sifat_surat'].'</option>';				
													}
												?>
											</select>
										</div>
									</div>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Prihal</label>
										<div class="col-sm-10">
											<input name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text" required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Keterangan</label>
										<div class="col-sm-10">
											<textarea name="keterangan" placeholder="Keterangan" class="form-control" ></textarea>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Pilih File</label>
										<div class="col-sm-10">
											<input type="file" name="img" id="img" class="form-control" required>
										</div>
									</div>
									<!-- Text input-->
									<!--Tabs Disposisi Surat-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Ditujukan Ke</label>
											<div class="col-sm-10">
												<select id="disposisi2" name="disposisi2[]" class="js-example-responsive" multiple="multiple" style="width: 100%" required>
													<option class="form-control" ></option>
														<?php
															$tampil=mysql_query("select * from tbl_staf order by id_staf asc");
															while ($data = mysql_fetch_array($tampil))
															{
														?>
															<option class="form-control" value= "<?php echo $data['id_staf']; ?>"> <?php echo $data['nama']; ?>
															</option>
														<?php	
															}
														?>
												</select>	
											</div>
											
											<!--java Scrip untuk select2-->
											<script>
												$("#disposisi2").select2({
														placeholder: "Please Select"
													});
											</script>
											<!--End java Scrip untuk select2-->
									</div>
									<!--Tabs Disposisi Surat-->
									
								</fieldset>
								<div class="modal-footer">
									<button name="btnAdd4" class="btn btn-primary" type="submit" >Simpan</button>
									<a href="index.php"><button type="button" class="btn btn-danger" data-dismiss="">Batal</button></a>											
								</div>
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- And Modal Surat Keluar -->
	
	<!-- PHP Simapan Surat Keluar -->
	<?php
		error_reporting(0);
		if (isset($_POST['btnAdd4'])) {
			$tgl_terima=date('y-m-d');
			
			//<!-- PHP Simapan Images -->
			$ekstensi_diperbolehkan	= array('png','jpg','pdf');
			$folder="../uploads/";
			$img = rand(1000,100000)."-".$_FILES['img']['name'];
			$x = explode('.', $img);
			$ekstensi = strtolower(end($x));
			$img_loc = $_FILES['img']['tmp_name'];		
			if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
				move_uploaded_file($img_loc,$folder.$img);
	
			}
			else{
					echo 'EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
				}
			//<!-- PHP Simapan Images -->
			
			  $insert_query = mysql_query("INSERT INTO tbl_surat(no_surat,id_sifat,surat_dari,prihal,tgl_surat,keterangan,jenis_surat,lampiran) VALUES ('$_POST[no_surat]','$_POST[sifat_surat]','$_POST[surat_dari]','$_POST[prihal]','$_POST[tgl_surat]','$_POST[keterangan]','Keluar','$img')");
			  if($insert_query) {
				  if(count($_POST['disposisi2'])){
					  foreach($_POST['disposisi2'] as $row){
						 $insert_query = mysql_query(" insert into tbl_disposisi(no_surat,id_staf,status_terbaca) values ('$_POST[no_surat]','$row','Tidak' ) ");
					  }
				  }
					echo "<script language='javascript'>alert('Data Surat Berhasil Disimpan'); document.location='../admin/kirim.php?no_surat=$_POST[no_surat]';</script>"; 			
			  } 
				else
					{
						echo '<script language="javascript">alert("Data Surat Gagal Disimpan"); document.location="../admin/surat.php";</script>';  
					}
		}
	?>
	<!-- End PHP Simapan Surat Keluar -->
	
	<!-- Modal Tulis Surat -->
	<?php 
		$tampil="SELECT id_staf FROM tbl_staf INNER JOIN tbl_jabatan USING (id_jabatan) WHERE jabatan ='Ketua' ";
		$result_tampil = mysql_query($tampil);
		$count_tampil = mysql_num_rows($result_tampil);
		while ($data = mysql_fetch_array($result_tampil)) {
		$ketua = $data['id_staf'];
		}
	?>
	<div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabe3">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			<form method="POST" action="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myLargeModalLabel3">Form Tulis Surat</h4>
				</div>
				<div class="modal-body">
					<?php 
						//<!--Untuk Konversi Bulan Ke Romawi-->
					    $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
						$romawi = $array_bulan[date('n')];		
						//<!--End Untuk Konversi Bulan Ke Romawi-->
						
						//<!--Untuk Mengambil 2 Angka Tahun Sekarang-->
						$tahun=date("y");
						//<!--End Untuk Mengambil 2 Angka Tahun Sekarang-->
						
						//<!--Untuk Kode otomatis-->
						$waktu_sekarang = date('Y-m-d');
						$bulan_sekarang = date('Y-m');
						$data = mysql_query("SELECT max(no_surat) As no_surat FROM tbl_surat where jenis_surat='Keluar' and tgl_surat LIKE '%$bulan_sekarang%' " );
						while ($dataTampil = mysql_fetch_array($data)) {
						$HasilMax=$dataTampil['no_surat'];
						//memotong string dengan Explode
						$potongan_NoSurat= explode (".",$HasilMax);
						//And memotong string dengan Explode
						
						//Membuat Variabel Untuk Menampung hAsil Potongan
						$HasilPotong = $potongan_NoSurat[3];
						//And Membuat Variabel Untuk Menampung hAsil Potongan
						
						//Menambahkan Nilai Satu Untuk Kode Otomatis
						$no_surat2=$HasilPotong+1;
						//And Menambahkan Nilai Satu Untuk Kode Otomatis
						
						//Pengkondisian Untuk Penambahan Angka 0
						if (strlen($no_surat2)==1)
						{
							$no_surat2='0'.$no_surat2;
						}
						//And Pengkondisian Untuk Penambahan Angka 0
						
						//<!--End Untuk Kode Otomatis-->
						
					?>
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
								<fieldset>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Nomor Surat</label>
										<div class="col-sm-10">
											<input name="no_surat" id="no_surat" placeholder="Input No Surat" class="form-control" type="text" 
											value="<?php echo "DL.02.02.".$no_surat2.".TU.".$romawi.".".$tahun; ?>" readonly="readonly" required >
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
										<div class="col-sm-4">
											<select name="sifat_surat" required>
												<?php
													include("../assets/koneksi/koneksi.php");
													$query = "select * from tbl_sifat_surat";
													echo "<option value='' selected>--Pilih Sifat Surat--</option>";
													$hasil = mysql_query($query);
													while ($qtabel = mysql_fetch_assoc($hasil))
													{
														echo '<option value="'.$qtabel['id_sifat'].'">'.$qtabel['sifat_surat'].'</option>';				
													}
												?>
											</select>
										</div>
									</div>
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Prihal</label>
										<div class="col-sm-10">
											<input name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text"  required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Kepada</label>
										<div class="col-sm-10">
											<input name="kepada" type="text" placeholder="Kepada Yth." class="form-control"  type="text"  required>
										</div>
									</div>
									<!-- Text input-->
									<!-- Text input-->
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textinput">Isi Surat</label>
										<div class="col-sm-10">
											<textarea name="isi" placeholder="Isi Surat"  class="isi" ></textarea>
										</div>
									</div>
									<!-- Text input-->									
								</fieldset>
								<div class="modal-footer">
									<button name="btnAdd2" class="btn btn-primary" type="submit">Simpan</button>
									<a href="index.php"><button type="button" class="btn btn-danger" data-dismiss="">Batal</button></a>													
								</div>
						</div>
						<!-- /.col-lg-12 -->
					</div><?php } ?>
					<!-- /.row -->
				</div>
			</form>
			</div>
		</div>
	</div>
	<!-- And Modal Tulis Surat -->
	
	
	<!-- PHP Untuk Insert Data Surat -->
	<?php
		if (isset($_POST['btnAdd2'])) {
				$kepada="Yth. ".$_POST['kepada'];
				$insert_query = mysql_query("INSERT INTO tbl_surat(no_surat,id_sifat,surat_dari,prihal,kepada,tgl_surat,tgl_terima,konten,jenis_surat,approve) VALUES ('$_POST[no_surat]','$_POST[sifat_surat]','TU','$_POST[prihal]','$kepada','$waktu_sekarang','$waktu_sekarang','$_POST[isi]','Keluar','Tidak')");
				
				if($insert_query) {
					echo "<script language='javascript'>alert('Data Surat Berhasil Disimpan'); document.location='../admin/kirim_approve.php?no_surat=$_POST[no_surat]';</script>";   
				} 
					else
						{
							echo '<script language="javascript">alert("Data Surat Gagal Disimpan"); document.location="../admin/surat.php";</script>';  
						}
			}
	?>
	<!-- And PHP Untuk Insert Data Surat -->
	
	<!-- PHP Untuk Insert Data Surat -->
	<?php
		if (isset($_POST['btnAdd2'])) {
				$insert_query = mysql_query("INSERT INTO tbl_disposisi(no_surat,id_staf,status_terbaca) VALUES ('$_POST[no_surat]','$ketua','Tidak')");
				if($insert_query) {
					echo "<script language='javascript'>alert('Data Surat Berhasil Disimpan'); document.location='../admin/kirim_approve.php?no_surat=$_POST[no_surat]';</script>"; 						
				} 
					else
						{
							echo '<script language="javascript">alert("Data Surat Gagal Disimpan"); document.location="../admin/surat.php";</script>';  
						}
			}
	?>
	<!-- And PHP Untuk Insert Data Surat -->
	
	<!--Javascrip Tynimce-->
		<script type="text/javascript">
			tinymce.init({
			selector: ".isi",
			height : 400,
			// ===========================================
			// INCLUDE THE PLUGIN
			// ===========================================
            
			// ===========================================
			// PUT PLUGIN'S BUTTON on the toolbar
			// ===========================================
            
			toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | fontselect | fontsizeselect",

			// ===========================================
			// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
			// ===========================================
				});
		</script>
	<!--Javascrip Tynimce-->
	
	<!--Javascrip Datepicker-->
	<script>
		$(function () {
		$("#date1").datepicker({ 
				autoclose: true, 
				format: 'yyyy-mm-dd',
				autoclose: true,
		}).datepicker(new Date());;
		});
	</script>
	
	<script>
		$(function () {
		$("#date2").datepicker({ 
				autoclose: true, 
				format: 'yyyy-mm-dd',
				autoclose: true,
		}).datepicker(new Date());;
		});
	</script>
	<!--End Javascrip Datepicker-->
	
	<!-- Modal Informasi -->
	<div class="modal bs-example-modal-md fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel3">Prosedur Pendataan</h4>
				</div>
				<div class="modal-body">
						<table class="table table-hover">
							<tr>
								<b>
									<p>1. Login Dengan Username dan Password </p>
									<p>2. Lengkapi Semua Formulir Dengan Benar</p>
									<p>3. Centang Cekbox Validasi Pendaftaran</p>
									<p>4. Klick Button Simpan</p>
								</b>
							</tr>
						</table>
					<!-- /.row -->
				</div>
				<div class="modal-footer">
					<b>Untuk Lebih Jelas Silakan Download Panduannya disini</b>&nbsp;&nbsp;
					<button type="button" class="btn btn-primary" >Download</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>												
				</div>
			</div>
		</div>
	</div>
	<!-- And Modal Informasi -->
  </div>
</nav>    