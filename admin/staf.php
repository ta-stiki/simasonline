<?php ob_start();?>
<?php session_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['level'] == "Admin"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!-- Bootstrap core CSS -->
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				<link href="../assets/css/custom.css" rel="stylesheet">
				<!-- Datatabel Plugin -->
				<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
				<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
				<script src="../datatabel/js/jquery.dataTables.js"></script>
				<script src="../datatabel/js/fixedtabel.min.js"></script>
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Master Data Staf</b></h4>
								</header>
							</div>
						</center>
						</br>
						<form method="POST" action="">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button name="btnNew" class="btn btn-primary" type="submit">Tambah Data Staf</button>
						</form>
														
						<!-- Modal Tamabh Staf -->
						<?php if (isset($_POST['btnNew'])) { ?>
						<div class="modal fade bs-example-modal-lg" id="addModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
								<form method="POST" action="">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Tambah Data Staf</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Id Staf</label>
															<div class="col-sm-10">
																<input name="id_staf" id="id_staf" placeholder="Id Staf" class="form-control" type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Nama</label>
															<div class="col-sm-10">
																<input name="nama" id="nama" placeholder="Input Nama Staf" class="form-control" type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Jabatan</label>
															<div class="col-sm-4">
																<select name="jabatan">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_jabatan";
																		echo "<option value='' selected>--Pilih Jabatan--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			echo '<option value="'.$qtabel['id_jabatan'].'">'.$qtabel['jabatan'].'</option>';				
																		}
																	?>
																</select>
															</div>
															<label class="col-sm-2 control-label" for="textinput">Prodi</label>
															<div class="col-sm-4">
																<select name="prodi">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_prodi";
																		echo "<option value='' selected>--Pilih Prodi--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			echo '<option value="'.$qtabel['id_prodi'].'">'.$qtabel['prodi'].'</option>';				
																		}
																	?>
																</select>
															</div>
														</div>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Alamat</label>
															<div class="col-sm-10">
																<input type="text" name="alamat" placeholder="Alamat" class="form-control"  type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No Telp</label>
															<div class="col-sm-10">
																<input type="text" name="telepon" placeholder="No Telp" maxlength="12" pattern=".{11,}" class="form-control" id="tlp"  type="text" required>
																<script>
																	$("#tlp").keypress(function (e) {
																	//if the letter is not digit then display error and don't type anything
																	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
																	//display error message
																	return false;
																	}
																	});
																</script>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">E-Mail</label>
															<div class="col-sm-10">
																<input type="email" name="email" placeholder="E-mail" class="form-control"  type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Username</label>
															<div class="col-sm-10">
																<input type="text" name="username" placeholder="Username" class="form-control"  type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Password</label>
															<div class="col-sm-10">
																<input type="text" name="password" placeholder="Password" class="form-control"  type="text" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Level</label>
																<div class="col-sm-4">
																	<select name="level" required>
																		<option value="">Pilih Level</option>  
																		<option value="Admin">Admin</option>  
																		<option value="Ketua">Ketua</option>  
																		<option value="Staf">Staf</option>  
																	</select>
																</div>
														</div>
														<!-- Text input-->
													</fieldset>
													<div class="modal-footer">
														<button name="btnAdd" class="btn btn-primary" type="submit">Simpan</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>												
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
								</form>
								</div>
							</div>
						</div>
						<?php } ?>
						<!-- And Modal Tamabah Staf -->
						
						<div id="batas" class="module_content">
							
								<div class="table-responsive">
									<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
										<thead>
											<tr bgcolor="#F5F5F5">
												<th>No </th>
												<th>Nama &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Id &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Jabatan &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Prodi &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>No. Telp &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Email &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Alamat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Username &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Password &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Level &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
										include("../assets/koneksi/koneksi.php");									
										?>
											<?php
											$view=mysql_query("select tbl_staf.*										
												,tbl_jabatan.jabatan
												,tbl_prodi.prodi 
												from tbl_staf
												INNER JOIN tbl_jabatan
												ON (tbl_staf.id_jabatan = tbl_jabatan.id_jabatan)
												INNER JOIN tbl_prodi
												ON (tbl_staf.id_prodi = tbl_prodi.id_prodi) 
												");
											$no=0;
											while($row=mysql_fetch_array($view)){
												$no++;
											?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $row['nama'];?></td>
													<td><?php echo $row['id_staf'];?></td>
													<td><?php echo $row['jabatan'];?></td>
													<td><?php echo $row['prodi'];?></td>
													<td><?php echo $row['no_tlp'];?></td>
													<td><?php echo $row['email'];?></td>
													<td><?php echo $row['alamat'];?></td>
													<td><?php echo $row['username'];?></td>
													<td><?php echo $row['password'];?></td>
													<td><?php echo $row['level'];?></td>
													<td><?php echo $row['status'];?></td>
													<td> 
														<form method="POST" action="">
															<input type="hidden" name="id_staf" value="<?php echo $row['id_staf']; ?>">
															<button name="btnEdit" class="btn btn-success" type="submit">Ubah</button>
														</form>
													</td>
												</tr>
											<?php
											}
											?>
										</tbody>
										<!-- JavaScript Untuk datatabel scroll-->
										<script>
											$(document).ready(function() {
												$('#example').DataTable( {
													"scrollY": 200,
													"scrollX": true,
													fixedColumns:   {
													"leftColumns": 2,
													"rightColumns": 1
												}
												} );
											} );
										</script>
										<!-- end JavaScript Untuk datatabel scroll-->
									</table>
								</div>
						</div>
					</article>
				</div>
								
				<!-- Modal Edit Staf -->
				<?php if (isset($_POST['btnEdit'])) { ?>
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Staf</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_staf 
																inner join tbl_jabatan on (tbl_staf.id_jabatan=tbl_jabatan.id_jabatan) 
																inner join tbl_prodi on (tbl_staf.id_prodi=tbl_prodi.id_prodi)
																WHERE id_staf='$_POST[id_staf]' ");
										  while ($show = mysql_fetch_array($shows)) { 
											?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Id Staf</label>
															<div class="col-sm-10">
																<input type="hidden" name="id" value="<?php echo $show['id_staf']; ?>">
																<input name="id_staf" id="id_staf" placeholder="Id Staf" class="form-control" type="text" value="<?php echo $show['id_staf']; ?>" readonly = "readonly" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Nama</label>
															<div class="col-sm-10">
																<input name="nama" id="nama" placeholder="Input Nama Staf" class="form-control" type="text" value="<?php echo $show['nama']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Jabatan</label>
															<div class="col-sm-4">
																<select name="jabatan">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_jabatan";
																		echo "<option value='' selected>--Pilih Jabatan--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			$select = ($qtabel['id_jabatan'] == $show['id_jabatan'])? "selected":null;
																			echo '<option '.$select.' value="'.$qtabel['id_jabatan'].'">'.$qtabel['jabatan'].'</option>';				
																		}
																	?>
																</select>
															</div>
															<label class="col-sm-2 control-label" for="textinput">Prodi</label>
															<div class="col-sm-4">
																<select name="prodi">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_prodi";
																		echo "<option value='' selected>--Pilih Prodi--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			$select = ($qtabel['id_prodi'] == $show['id_prodi'])? "selected":null;
																			echo '<option '.$select.' value="'.$qtabel['id_prodi'].'">'.$qtabel['prodi'].'</option>';				
																		}
																	?>
																</select>
															</div>
														</div>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Alamat</label>
															<div class="col-sm-10">
																<input type="text" name="alamat" placeholder="Alamat" class="form-control"  type="text" value="<?php echo $show['alamat']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No Telp</label>
															<div class="col-sm-10">
																<input type="text" name="telepon" maxlength="12" pattern=".{11,}"  placeholder="No Telp" class="form-control" id="tlp"  type="text" value="<?php echo $show['no_tlp']; ?>" required>
																<script>
																	$("#tlp").keypress(function (e) {
																	//if the letter is not digit then display error and don't type anything
																	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
																	//display error message
																	return false;
																	}
																	});
																</script>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">E-Mail</label>
															<div class="col-sm-10">
																<input type="email" name="email" placeholder="E-mail" class="form-control"  type="text" value="<?php echo $show['email']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Username</label>
															<div class="col-sm-10">
																<input type="text" name="username" placeholder="Username" class="form-control"  type="text" value="<?php echo $show['username']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Password</label>
															<div class="col-sm-10">
																<input type="text" name="password" placeholder="Password" class="form-control"  type="text" value="<?php echo $show['password']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Level</label>
																<div class="col-sm-4">
																	<select name="level" required>
																		<option></option>
																		<?php
																		if ($show['level'] == 'Ketua') {
																			echo "<option value='Admin'>Admin</option>";
																			echo "<option value='Ketua' selected>Ketua</option>";
																			echo "<option value='Staf'>Staf</option>";
																		  }
																		  if ($show['level'] == 'Admin') {
																			echo "<option value='Admin' selected>Admin</option>";
																			echo "<option value='Ketua'>Ketua</option>";
																			echo "<option value='Staf'>Staf</option>";
																		  }
																		  
																		  if ($show['level'] == 'Staf') {
																			echo "<option value='Admin'>Admin</option>";
																			echo "<option value='Ketua'>Ketua</option>";
																			echo "<option value='Staf' selected>Staf</option>";
																		  }
																		 ?>
																	  </select>																
																</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Status</label>
																<div class="col-sm-4">
																	<select name="status" required>
																		<option></option>
																		<?php
																		 $shows = mysql_query("SELECT status FROM tbl_staf 
																			WHERE id_staf='$_POST[id_staf]' ");
																			while ($show2 = mysql_fetch_array($shows)) { 
																		if ($show2['status'] == 'Aktif') {
																			echo "<option value='Aktif' selected>Aktif</option>";
																			echo "<option value='Tidak Aktif'>Tidak Aktif</option>";
																		  }
																		if ($show2['status'] == 'Tidak Aktif') {
																			echo "<option value='Aktif'>Aktif</option>";
																			echo "<option value='Tidak Aktif' selected>Tidak Aktif</option>";
																		  }
																		 ?>
																	  </select>																
																</div>
														</div>
														<?php }?>
														<!-- Text input-->
													</fieldset>
													<div class="modal-footer">
														<button name="btnUpdate" class="btn btn-primary" type="submit">Simpan</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
													<?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Staf -->
				<?php } ?>				
				
				<!-- PHP Untuk Insert Data Staf -->
				<?php
					if (isset($_POST['btnAdd'])) {
						  $insert_query = mysql_query("INSERT INTO tbl_staf VALUES ('$_POST[id_staf]','$_POST[jabatan]','$_POST[prodi]','$_POST[nama]','$_POST[alamat]','$_POST[telepon]','$_POST[email]','Aktif','$_POST[username]','$_POST[password]','$_POST[level]')");
						  if($insert_query) {
							echo '<script language="javascript">alert("Data Staf Berhasil Disimpan"); document.location="../admin/staf.php";</script>'; 
						  } 
							else
								{
									echo '<script language="javascript">alert("Data Staf Gagal Disimpan"); document.location="../admin/staf.php";</script>';  
								}
					}
				?>
				<!-- And PHP Untuk Insert Data Staf  -->
				
				<!-- PHP Untuk Update Data Staf  -->
				<?php
					if (isset($_POST['btnUpdate'])) {
						  $update_query = mysql_query("UPDATE tbl_staf SET nama='$_POST[nama]', id_jabatan='$_POST[jabatan]', id_prodi='$_POST[prodi]', alamat='$_POST[alamat]', no_tlp='$_POST[telepon]', email='$_POST[email]', username='$_POST[username]', password='$_POST[password]', level='$_POST[level]', status='$_POST[status]' WHERE id_staf='$_POST[id]' ");
						  if($update_query) {
							echo "<script type='text/javascript'>alert('Data Staf Berhasil Diubah')</script>";
							echo "<script>setTimeout(\"location.href = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]';\",0);</script>";
						  } 
						  else
							{
								echo '<script language="javascript">alert("Data Staf Gagal Diubah"); document.location="../admin/staf.php";</script>';  
							}
					}
				?>
				<!-- And PHP Untuk Update Data Staf  -->
								
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				<!--JavaScript Untuk datatabel-->
				<script>
				$(document).ready(function() {
				$('#example').DataTable();
				} );
				</script>
				
				<script type="text/javascript">
				  // SHOW EDIT MODAL ON LOAD
					$(window).load(function(){
						$("#editModal").modal("show");
						$("#addModal").modal("show");
					});
				</script>
				<!-- End JavaScript Untuk datatabel-->
				
			</body>
		</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>