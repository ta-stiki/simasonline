<?php ob_start();?>
<?php session_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['level'] == "Admin"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!--Select2 Plugin-->
				<link href="../select2-master/dist/css/select2.min.css" rel="stylesheet">
				
				<!-- Bootstrap core CSS -->
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				<script src="../select2-master/dist/js/select2.min.js"></script>	<!--Select2 Plugin-->
				<link href="../assets/css/custom.css" rel="stylesheet">
				<script type="text/javascript" src="../assets/tinymce/tinymce.min.js"></script>
				<!-- Datatabel Plugin -->
				<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
				<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
				<script src="../datatabel/js/jquery.dataTables.js"></script>
				<script src="../datatabel/js/fixedtabel.min.js"></script>
				<!--Datepicker Plugin-->
				<script src="../assets/js/bootstrap-datepicker.js"></script>
				<link href="../assets/css/datepicker.css" rel="stylesheet">				
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Data Surat</b></h4>
								</header>
							</div>
						</center>
						</br>
																
						<div id="batas" class="module_content">
							
								<div class="table-responsive">
									<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
										<thead>
											<tr bgcolor="#F5F5F5">
												<th>No </th>
												<th>Surat Dari &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>No Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Prihal &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Sifat Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Terima &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Nama Disposisi &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Jenis Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status Terbaca &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Terbaca  &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status Approve &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
												include("../assets/koneksi/koneksi.php");									
											?>
											<?php
												$view=mysql_query("SELECT *
																		FROM tbl_surat INNER JOIN tbl_disposisi on tbl_surat.no_surat=tbl_disposisi.no_surat
																		inner join tbl_staf on tbl_staf.id_staf=tbl_disposisi.id_staf
																		inner join tbl_sifat_surat on tbl_surat.id_sifat=tbl_sifat_surat.id_sifat
																		order by tbl_surat.jenis_surat ASC, tbl_disposisi.status_terbaca DESC;
																	");
												$no=0;
												while($row=mysql_fetch_array($view)){
												$no++;
											?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $row['surat_dari'];?></td>
													<td><?php echo $row['no_surat'];?></td>
													<td><?php echo $row['prihal'];?></td>
													<td><?php echo $row['sifat_surat'];?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_surat']));?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_terima']));?></td>
													<td><?php echo $row['nama'];?></td>
													<td><?php echo $row['jenis_surat'];?></td>
													<td><?php echo $row['status_terbaca'];?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_terbaca']));?></td>
													<td><?php echo $row['approve'];?></td>
													<td> 
														<form method="POST" action="">
															<input type="hidden" name="no_surat" value="<?php echo $row['no_surat']; ?>">
															<input type="hidden" name="id_staf" value="<?php echo $row['id_staf']; ?>">
															<?php if ($row['approve']=='Ya') {
																?><button name="btnEditYa" class="btn btn-success" type="submit">Ubah</button>&nbsp;|&nbsp;<?php
															} else {
																?><button name="btnEdit" class="btn btn-success" type="submit">Ubah</button>&nbsp;|&nbsp;<?php
															} ?>
															
															
															<?php if ($row['lampiran']) { ?>
															<a href="../uploads/<?php echo $row['lampiran']; ?>" target="_blank"><button class="btn btn-primary" type="button" >View</button></a>
															<?php } ?>
															<?php if ($row['konten']) { ?>
															<a href="view_surat.php?no_surat=<?php echo $row['no_surat']; ?>" target="_blank"><button name="btnView" class="btn btn-danger" type="button">View</button>
															<?php } ?>
														</form>
													</td>
												</tr>
												<?php
												}
												?>
										</tbody>
										<!-- JavaScript Untuk datatabel scroll-->
										<script>
											$(document).ready(function() {
												$('#example').DataTable( {
													"scrollY": 200,
													"scrollX": true,
													fixedColumns:   {
													"leftColumns": 2,
													"rightColumns": 1
												}
												} );
											} );
										</script>
										<!-- end JavaScript Untuk datatabel scroll-->
									</table>
								</div>
						</div>
					</article>
				</div>
											
				<!-- Modal Edit Surat -->
				<?php if (isset($_POST['btnEdit'])) { ?>
					
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="" enctype="multipart/form-data" >
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Surat</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_surat INNER JOIN tbl_sifat_surat 
											ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)  WHERE no_surat='$_POST[no_surat]' ");
										  while ($show = mysql_fetch_array($shows)) { 
										  $image=$show['lampiran'] ;
										?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No_surat</label>
															<div class="col-sm-10">
																<input type="hidden" name="no_surat" value="<?php echo $show['no_surat']; ?>">
																<input name="no_surat" id="no_surat" placeholder="Nomor Surat" class="form-control" type="text" value="<?php echo $show['no_surat']; ?>" readonly="readonly" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Surat Dari</label>
														<div class="col-sm-10">
															<input name="surat_dari" id="dari" placeholder="Surat Dari" class="form-control" type="text" value="<?php echo $show['surat_dari']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput" >Tgl. Surat</label>
														<div class="col-sm-4">
															<input name="tgl_surat" type="text" placeholder="Tanggal Surat" class="form-control" id="date3" name="date3" value="<?php echo $show['tgl_surat']; ?>" required>
														</div>
														<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
														<div class="col-sm-4">																
																<select name="sifat_surat" required>
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_sifat_surat";
																		echo "<option value='' selected>--Pilih Sifat Surat--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			$select = ($qtabel['id_sifat'] == $show['id_sifat'])? "selected":null;
																			echo '<option '.$select.' value="'.$qtabel['id_sifat'].'">'.$qtabel['sifat_surat'].'</option>';				
																		}
																	?>
																</select>																									
														</div>
													</div>
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Prihal</label>
														<div class="col-sm-10">
															<input name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text" value="<?php echo $show['prihal']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<?php if ($show['kepada']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Kepada</label>
														<div class="col-sm-10">
															<input name="kepada" type="text" placeholder="Kepada" class="form-control"  type="text" value="<?php echo $show['kepada']; ?>" required>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['konten']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Isi Surat</label>
														<div class="col-sm-10">
															<textarea name="isi" class="isi" ><?php echo $show['konten']; ?></textarea>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<?php if ($show['keterangan']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Keterangan</label>
														<div class="col-sm-10">
															<textarea name="keterangan" placeholder="Keterangan" class="form-control" ><?php echo $show['keterangan']; ?></textarea>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['lampiran']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Pilih File</label>
														<div class="col-sm-10">
															<input type="file" name="img" id="img" class="form-control" value="<?php echo $image ?>" />
															<a href="../uploads/<?php echo $image ?>" target="_blank"><?php echo $image ?></a>
														</div>
													</div>
													<?php } ?>
													
													<!-- PHP Select Tangging/get-->						
													<?php
														$disposisiAwal = [];
														$shows2 = mysql_query("SELECT * FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]' ");
														while($show2=mysql_fetch_array($shows2)){
															$disposisiAwal[]= $show2['id_staf'];
														}
													?>														
													<!-- PHP Select Tangging/get-->		
													
													<!--Tabs Disposisi Surat-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Disposisi Ke</label>
															<div class="col-sm-10">
																<select id="disposisi3" name="disposisi3[]" class="js-example-responsive" multiple="multiple" style="width: 100%" >
																	<option class="form-control" ></option>
																		<?php
																			$tampil=mysql_query("select * from tbl_staf order by id_staf asc");
																			while ($data = mysql_fetch_array($tampil))
																			{
																				foreach($disposisiAwal as $staff){
																					if ($data['id_staf']==$staff){
																						echo '<option class="form-control" value= "'.$data['id_staf'].'"  selected="selected">'.$data['nama'].'</option>';
																					}
																					else{
																						echo '<option class="form-control" value= "'.$data['id_staf'].'">'.$data['nama'].'</option>';
																					}
																				}
																			}
																		?>	
																</select>	
															</div>
															
															<!--java Scrip untuk select2-->
															<script>
																$(document).ready(function(){
																	$("#disposisi3").select2({
																		placeholder: "Please Select",
																		tags: true,
																		tokenSeparators: [','],
																	});
																})
																
															</script>
															<!--End java Scrip untuk select2-->
													</div>
													<!--Tabs Disposisi Surat-->
													</fieldset>
													<div class="modal-footer">
														<?php if ($show['konten']) { ?>
															<button name="btnUpdate2" class="btn btn-success" type="submit">Update</button>
														<?php } ?>
														<?php if ($show['lampiran']) { ?>
														<button name="btnUpdate" class="btn btn-primary" type="submit">Update</button>
														<?php } ?>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
										  <?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Surat -->						
				<?php } ?>	









				<!-- Modal Edit Surat -->
				<?php if (isset($_POST['btnEditYa'])) { ?>
					
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="" enctype="multipart/form-data" >
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Surat</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_surat INNER JOIN tbl_sifat_surat 
											ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)  WHERE no_surat='$_POST[no_surat]' ");
										  while ($show = mysql_fetch_array($shows)) { 
										  $image=$show['lampiran'] ;
										?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No_surat</label>
															<div class="col-sm-10">
																<input type="hidden" name="no_surat" value="<?php echo $show['no_surat']; ?>">
																<input name="no_surat" id="no_surat" placeholder="Nomor Surat" class="form-control" type="text" value="<?php echo $show['no_surat']; ?>" readonly="readonly" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Surat Dari</label>
														<div class="col-sm-10">
															<input readonly="readonly" name="surat_dari" id="dari" placeholder="Surat Dari" class="form-control" type="text" value="<?php echo $show['surat_dari']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Tgl. Surat</label>
														<div class="col-sm-4">
															<input readonly="readonly" name="tgl_surat" type="text" placeholder="Tanggal Surat" class="form-control" id="date3" name="date3" value="<?php echo $show['tgl_surat']; ?>" required>
														</div>
														<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
														<div class="col-sm-4">		
																<?php
																	include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_sifat_surat WHERE id_sifat='$show[id_sifat]' ";
																	    $hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_array($hasil))
																		{
																			$sifat = $qtabel['sifat_surat'];				
																		}
																?>
																<input type="hidden" name="sifat_surat" value="<?php echo $show['id_sifat'] ?>">
																<input type="text" class="form-control" readonly="readonly" name="" value="<?php echo $sifat ?>">
																																									
														</div>
																																									
														</div>
													</div>
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Prihal</label>
														<div class="col-sm-10">
															<input readonly="readonly" name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text" value="<?php echo $show['prihal']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<?php if ($show['kepada']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Kepada</label>
														<div class="col-sm-10">
															<input readonly="readonly" name="kepada" type="text" placeholder="Kepada" class="form-control"  type="text" value="<?php echo $show['kepada']; ?>" required>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['konten']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Isi Surat</label>
														<div class="col-sm-10">
															<textarea name="isi" class="hide" ><?php echo $show['konten']; ?></textarea>
															<div class="well" ><?php echo $show['konten']; ?></div>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<?php if ($show['keterangan']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Keterangan</label>
														<div class="col-sm-10">
															<textarea readonly="readonly" name="keterangan" placeholder="Keterangan" class="form-control" ><?php echo $show['keterangan']; ?></textarea>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['lampiran']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Pilih File</label>
														<div class="col-sm-10">
															<input readonly="readonly" type="file" name="img" id="img" class="form-control" value="<?php echo $image ?>" />
															<a href="../uploads/<?php echo $image ?>" target="_blank"><?php echo $image ?></a>
														</div>
													</div>
													<?php } ?>
													
													<!-- PHP Select Tangging/get-->						
													<?php
														$disposisiAwal = [];
														$shows2 = mysql_query("SELECT * FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]' ");
														while($show2=mysql_fetch_array($shows2)){
														$disposisiAwal[]= $show2['id_staf'];
													}
													?>														
													<!-- PHP Select Tangging/get-->		
													
													<!--Tabs Disposisi Surat-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Disposisi Ke</label>
															<div class="col-sm-10">
															    <label><input type="radio" class="dc" name="penerima_radio" value="pilih" checked>Pilih penerima</label>&nbsp;&nbsp;&nbsp;
																<label><input type="radio" class="dc" name="penerima_radio" value="semua">Kirim ke semua</label>
																<label class="hide"><input type="radio" class="dx" name="penerima_radios" value="<?php echo $show['no_surat']; ?>" checked><?php echo $show['no_surat']; ?></label>
																<!-- AJAX RADIO -->
																<script type="text/javascript">
																	var htmlobjek;
																	$(".dc").change(function(){
																		var dc = $(".dc:checked").val();
																		var dx = $(".dx:checked").val();
																		$.ajax({
																			type: "POST",
																			url: "get_penerima.php",
																			data: {penerima_radio:dc,penerima_radios:dx},
																			cache: false,
																			success: function(msg) {
																				$(".put-penerima").html(msg);
																			}
																		});
																	});
																</script>
																<!-- AJAX RADIO END -->
																<div class="put-penerima">
																<select id="disposisi3" name="disposisi3[]" class="js-example-responsive" multiple="multiple" style="width: 100%" >
																	<option class="form-control" ></option>
																		<?php
																			$tampil=mysql_query("select * from tbl_staf order by id_staf asc");
																			while ($data = mysql_fetch_array($tampil))
																			{
																				foreach($disposisiAwal as $staff){
																					if ($data['id_staf']==$staff){
																						echo '<option class="form-control" value= "'.$data['id_staf'].'"  selected="selected">'.$data['nama'].'</option>';
																					}
																					else{
																						echo '<option class="form-control" value= "'.$data['id_staf'].'">'.$data['nama'].'</option>';
																					}
																				}
																			}
																		?>	
																</select>	
																</div>
															</div>
															
															<!--java Scrip untuk select2-->
															<script>
																$(document).ready(function(){
																	$("#disposisi3").select2({
																		placeholder: "Please Select",
																		tags: true,
																		tokenSeparators: [','],
																	});
																})
																
															</script>
															<!--End java Scrip untuk select2-->
													</div>
													<!--Tabs Disposisi Surat-->
													</fieldset>
													<div class="modal-footer">
														<?php if ($show['konten']) { ?>
															<button name="btnUpdate2" class="btn btn-success" type="submit">Update</button>
														<?php } ?>
														<?php if ($show['lampiran']) { ?>
														<button name="btnUpdate" class="btn btn-primary" type="submit">Update</button>
														<?php } ?>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
										  <?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Surat -->						
				<?php } ?>	











				
				
				<!-- PHP Update Surat -->
				<?php
					error_reporting(0);
					$ekstensi_diperbolehkan	= array('png','jpg','pdf');
					$folder="../uploads/";
					$img = rand(1000,100000)."-".$_FILES['img']['name'];
					$x = explode('.', $img);
					$ekstensi = strtolower(end($x));
					$img_loc = $_FILES['img']['tmp_name'];		
					
					//Cek Apakah Btn Update Dipilih
					if (isset($_POST['btnUpdate'])) { 
						$tgl_terima=date('y-m-d');

						// Cek Apakah File Imaage Terisi
						if($_FILES['img']['name'] !== ""){
							if (in_array($ekstensi, $ekstensi_diperbolehkan) === false){
								echo '<script language="javascript">alert("Ektensi File Tidak Diijinkan !"); document.location="../admin/surat.php";</script>';  
							}
							
							//else
							
							if (in_array($ekstensi, $ekstensi_diperbolehkan) === true){
								if($_FILES['img']['size'] <= 100000){
									echo '<script language="javascript">alert("Upload Gagal, Ukuran File Melebihi 1 Megabyte !"); document.location="../admin/surat.php";</script>';  
								}
								else {
									move_uploaded_file($img_loc,$folder.$img);
									$insert_query = mysql_query("UPDATE tbl_surat SET id_sifat='$_POST[sifat_surat]', surat_dari='$_POST[surat_dari]'
																							, prihal='$_POST[prihal]', kepada='$_POST[kepada]'
																							, tgl_surat='$_POST[tgl_surat]', konten='$_POST[isi]'
																							, keterangan='$_POST[keterangan]'
																							,lampiran='$img' 
																							WHERE no_surat='$_POST[no_surat]' ");
								}
							}
							if($insert_query) {
								$insert_query = mysql_query("DELETE FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]'" 	); //menhapus telebih dahulu data disposisi
								$jml = count($_POST['disposisi3']);
								if(count($_POST['disposisi3'])){ 
									foreach($_POST['disposisi3'] as $staff){
										$insert_query = mysql_query("INSERT INTO tbl_disposisi (no_surat, id_staf) VALUES ('$_POST[no_surat]', '$staff') "); ///insert kembali data disposisi
									}
								}
								echo "<script language='javascript'>alert('Data Surat Berhasil Diubah'); document.location='../admin/kirim_ubah.php?no_surat=$_POST[no_surat]';</script>"; 
								//echo "Img=x".$_FILES['img']['name']."x";
							} 
								else
									{
										echo '<script language="javascript">alert("Data Surat Gagal Diubah"); document.location="../admin/surat.php";</script>';  
									}
						}// End Cek Apakah File Imaage Terisi
						
						// Cek Apakah File Imaage Kosong
						elseif($_FILES['img']['name'] == ""){
								$insert_query = mysql_query("UPDATE tbl_surat SET id_sifat='$_POST[sifat_surat]', surat_dari='$_POST[surat_dari]'
																					, prihal='$_POST[prihal]', kepada='$_POST[kepada]'
																					, tgl_surat='$_POST[tgl_surat]', konten='$_POST[isi]'
																					, keterangan='$_POST[keterangan]'
																					WHERE no_surat='$_POST[no_surat]' ");
								if($insert_query) {
									$insert_query = mysql_query("DELETE FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]'" 	); //menhapus telebih dahulu data disposisi
									$jml = count($_POST['disposisi3']);
									if(count($_POST['disposisi3'])){ 
										foreach($_POST['disposisi3'] as $staff){
											$insert_query = mysql_query("INSERT INTO tbl_disposisi (no_surat, id_staf) VALUES ('$_POST[no_surat]', '$staff') "); ///insert kembali data disposisi
										}
									}
									echo "<script language='javascript'>alert('Data Surat Berhasil Diubah'); document.location='../admin/kirim_ubah.php?no_surat=$_POST[no_surat]';</script>"; 
									//echo "Img=x".$_FILES['img']['name']."x";
								} 
									else
										{
											echo '<script language="javascript">alert("Data Surat Gagal Diubah"); document.location="../admin/surat.php";</script>';  
										}
						}// End Cek Apakah File Imaage Kosong
						
						
						if($show['konten'] !== ''){
							echo '<script language="javascript">alert("Uji Coba Konten"); document.location="../admin/surat.php";</script>';  
							
						}
						

				
					} // End Cek Btn Update Dipilih
				?>
				<!-- End PHP Update Surat -->
				
				<?php
					if (isset($_POST['btnUpdate2'])) {
						$terima = date('Y-m-d');
						
						$insert_query = mysql_query("UPDATE tbl_surat SET id_sifat='$_POST[sifat_surat]', surat_dari='$_POST[surat_dari]'
													, prihal='$_POST[prihal]', kepada='$_POST[kepada]'
													, tgl_surat='$_POST[tgl_surat]', konten='$_POST[isi]'
													, keterangan='$_POST[keterangan]'
													, tgl_terima='$terima'
													WHERE no_surat='$_POST[no_surat]' ");
						if($insert_query) {
							$insert_query = mysql_query("DELETE FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]'" 	); //menhapus telebih dahulu data disposisi
							$jml = count($_POST['disposisi3']);
							if(count($_POST['disposisi3'])){ 
								foreach($_POST['disposisi3'] as $staff){
									$insert_query = mysql_query("INSERT INTO tbl_disposisi (no_surat, status_terbaca, id_staf) VALUES ('$_POST[no_surat]', 'Tidak', '$staff') "); ///insert kembali data disposisi
								}
							}
							echo "<script language='javascript'>alert('Data Surat Berhasil Diubah'); document.location='../admin/kirim_ubah.php?no_surat=$_POST[no_surat]';</script>"; 
							//echo "Img=x".$_FILES['img']['name']."x";
						} 
							else
								{
									echo '<script language="javascript">alert("Data Surat Gagal Diubah"); document.location="../admin/surat.php";</script>';  
								}
					} 
				?>
								
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				<!--JavaScript Untuk datatabel-->
				<script>
				$(document).ready(function() {
				$('#example').DataTable();
				} );
				</script>
				
				<script type="text/javascript">
				  // SHOW EDIT MODAL ON LOAD
					$(window).load(function(){
						$("#editModal").modal("show");
						$("#addModal").modal("show");
					});
				</script>
				<!-- End JavaScript Untuk datatabel-->
				
				<!-- Javascrip Datepicker-->
				<script>
					$(function () {
					$("#date3").datepicker({ 
							autoclose: true, 
							format: 'yyyy-mm-dd',
							autoclose: true,
					}).datepicker(new Date());;
					});
				</script>
				<!--End Javascrip Datepicker-->
				
				<!--Javascrip Tynimce-->
				<script type="text/javascript">
					tinymce.init({
					selector: ".isi",
					height : 400,
					// ===========================================
					// INCLUDE THE PLUGIN
					// ===========================================
					
					// ===========================================
					// PUT PLUGIN'S BUTTON on the toolbar
					// ===========================================
					
					toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | fontselect | fontsizeselect",
		
					// ===========================================
					// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
					// ===========================================
						});
				</script>
				<!--Javascrip Tynimce-->
			</body>
		</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>