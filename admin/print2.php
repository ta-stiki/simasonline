<?php ob_start();?>
<?php session_start();?>
<?php
	include("../assets/koneksi/koneksi.php");
	error_reporting(0);
?>
<?php
$bulan2 = array(
    '01' => 'Januari',
    '02' => 'Februari',
    '03' => 'Maret',
    '04' => 'April',
    '05' => 'Mei',
    '06' => 'Juni',
    '07' => 'Juli',
    '08' => 'Agustus',
    '09' => 'September',
    '10' => 'Oktober',
    '11' => 'November',
    '12' => 'Desember',
);
if (isset($_SESSION['username']) and ($_SESSION['level'] == "Admin"))
	{?>
<html>
	<head>
		<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
        <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
		<!-- Bootstrap core CSS -->
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/css/print_laporan.css" rel="stylesheet" >
        <script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<link href="../assets/css/custom.css" rel="stylesheet">
		<script src="../assets/js/highcharts.js"></script>
		<script src="../assets/js/exporting.js"></script>
		<!-- Datatabel Plugin -->
		<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
		<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
		<script src="../datatabel/js/jquery.dataTables.js"></script>
		<script src="../datatabel/js/fixedtabel.min.js"></script>
	</head>
	<body onload="javascript:window.print()">
		<div id="main-wrapper">
			<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
				<!-- Text input Load Bulan dari database-->
				
				<!-- UNTUK GET DATA JIKA TOMBOL CETAK DIPILIH-->				
				<?php if ($_REQUEST['cetak'] == '1' ){
					$bulan= DATE ("m", strtotime ($_REQUEST['bulan']));
					$tahun=$_REQUEST['tahun'];
					$bulan3=$_REQUEST['bulan'];
					$saatini= date("Y-m", strtotime ($tahun.'-'.$bulan3));
				}?>
				<!-- END UNTUK GET DATA JIKA TOMBOL CETAK DIPILIH-->
                <div class="kop-surat">
                    <table width="100%">
                        <tr>
                            <td width="25" align="center"><img src="../assets/Images/IMG-20180805-WA0000a.png"></td>
                            <td width="70" align="center" class="kop-surat">
                                <h1>Laporan Surat Keluar</h1>
                                <p><b>SMK NEGERI I BEBANDEM</b></p>
                                <br>
                                <p><b>Periode Bulan : <?php echo $bulan2[$bulan3]; ?>,&nbsp;  Tahun : <?php echo $tahun; ?></b></p>
                            </td>
                        </tr>
                    </table>
                    <hr class="header-border">
                </div>
			</form>
						
			<!--Untuk Merelod Tampilan Berdasarkan Bulan dan Tahun-->
			<?php if ($_REQUEST['cetak'] == '1' ){
			$bulan=$_REQUEST['bulan'];
			$tahun=$_REQUEST['tahun'];
			if ((empty($bulan)) or (empty($tahun))) { ?>
				<div style="text-align: center;font-weight:bold;font-zise:18px">Filter Bulan dan Tahun Masih Kosong</div>
				<div style="text-align: center;font-weight:bold;font-zise:18px">Silakan Pilih Bulan dan Tahun Terlebih Dahulu</div>
			<?php }else{
			 
				//Membuat Query
				$q=mysql_query("SELECT sifat_surat AS Sifat,surat_dari,jenis_surat, COUNT(*) AS jumlah
							FROM tbl_surat INNER JOIN tbl_sifat_surat ON tbl_surat.id_sifat=tbl_sifat_surat.id_sifat 
							WHERE jenis_surat='Keluar' and tgl_surat LIKE '%$saatini%' AND tgl_surat LIKE '%$tahun%' GROUP BY sifat_surat
							");									
			?>
			
			<!--LOAD Grafik-->
			<script type="text/javascript">
				$(function () {
					$('#view').highcharts({
						chart: {
							type: 'column'
						},
						title: {
							text: 'Jumlah Surat Keluar Berdasarkan Sifat Surat'
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							categories: [
								'Sifat Surat'
							]
						},
						yAxis: {
							min: 0,
							title: {
								text: 'Total'
							}
						},
						plotOptions: {
											 series: {
												borderWidth: 0,
												dataLabels: {
													enabled: true,
													format: '{point.y}'
												}
											 }
										},
						series: [
						
						<?php
						while($r=mysql_fetch_array($q)){
							echo "{ name: '".$r[Sifat]."',data: [".$r[jumlah]."]},";
						}
						?>
						]
					});
				});
			</script>
				<div id="view" style="min-width: 310px; height: 400px; margin: 0 auto"></div>  	

				<!--Menampilkan Data Tabel Laporan-->
				<div id="batas" class="module_content">
						<div class="table-responsive">
							<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
								<thead>
									<tr bgcolor="#F5F5F5">
										<th>No </th>
										<th>Surat Dari </th>
										<th>No Surat</th>
										<th>Prihal </th>
										<th>Sifat Surat </th>
										<th>Tgl. Surat </th>
										<th>Tgl. Terima </th>
									</tr>
								</thead>
								<tbody>
								<?php
								include("../assets/koneksi/koneksi.php");									
								?>
									<?php
									$view=mysql_query("SELECT *
															FROM tbl_surat 
															inner join tbl_sifat_surat on tbl_surat.id_sifat=tbl_sifat_surat.id_sifat
															WHERE jenis_surat='Keluar' and tgl_surat LIKE '%$saatini%' AND tgl_surat LIKE '%$tahun%'
														");
									$no=0;
									while($row=mysql_fetch_array($view)){
										$no++;
									?>
										<tr>
											<td><?php echo $no;?></td>
											<td><?php echo $row['surat_dari'];?></td>
											<td><?php echo $row['no_surat'];?></td>
											<td><?php echo $row['prihal'];?></td>
											<td><?php echo $row['sifat_surat'];?></td>
											<td><?php echo date("d-m-Y",strtotime ($row['tgl_surat']));?></td>
											<td><?php echo date("d-m-Y",strtotime ($row['tgl_terima']));?></td>
										</tr>
									<?php
									}
									?>
								</tbody>
								<!-- JavaScript Untuk datatabel scroll-->
								<script>
									$(document).ready(function() {
										$('#example').DataTable( {
											"scrollY": 200,
											"scrollX": true
										}
										} );
									} );
								</script>
								<!-- end JavaScript Untuk datatabel scroll-->
							</table>
						</div>
				</div>
				<!--END Menampilkan Data Tabel Laporan-->
				
			<?php }
					}?>
			<!--END LOAD Grafik-->
			
		</div>
	</body>
</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>
