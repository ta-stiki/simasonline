<?php ob_start();?>
<?php session_start();  ?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="assets/css/akun.css"/>
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/Images/pavicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/Images/pavicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/Images/pavicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/Images/pavicon/favicon-16x16.png">
<title>Login</title>
</head>
<body>
<form method="post" action="">
	<div class="login">
		<div class="login-header">
			<h1>Login</h1>
		</div>
		<div class="login-form">
			<h3>Username:</h3>
			<input name="user_akun" size="10" value=""  type="text" placeholder="username"  required>
			<h3>Password:</h3>
			<input name="pass_akun" size="10" value=""  type="password" placeholder="password"  required>
			<button name="login">Login</button>
			<a href="javascript:close_window();"><button name="batal" type="submit">Batal</button></a>
			<br></br>
			<!--PHP untuk login-->
			<?php
                        ob_start();
				include("assets/koneksi/koneksi.php");
				if(isset($_POST['login'])!="")
					{
						
						$username = $_POST['user_akun']; //mengacu pada form login_akun
						$password = $_POST['pass_akun']; //mengacu pada form login_akun
						
						// query untuk mendapatkan record dari username
						$query = "SELECT * FROM tbl_staf WHERE username = '$username'";
						$hasil = mysql_query($query);
						$data = mysql_fetch_array($hasil);
						
						// cek kesesuaian password
						if ( ($password == $data['password']) and ($data['level'] == "Admin") )
						{
							//echo "<h1>Login Sukses</h1>";
						
							// menyimpan username dan level ke dalam session
							$_SESSION['level'] = $data['level'];
							$_SESSION['username'] = $data['username'];
							$_SESSION['id_staf'] = $data['id_staf'];
						ob_start();
							// tampilkan menu
							header("location: admin/index.php");						
						}
						else if ( ($password == $data['password']) and ($data['level'] == "Ketua") )
						{
							//echo "<h1>Login Sukses</h1>";
						
							// menyimpan username dan level ke dalam session
							$_SESSION['level'] = $data['level'];
							$_SESSION['username'] = $data['username'];
							$_SESSION['id_staf'] = $data['id_staf'];
						
							// tampilkan menu
							header("location: ketua/surat.php");						
						}
						else if ( ($password == $data['password']) and ($data['level'] == "Staf") )
						{
							//echo "<h1>Login Sukses</h1>";
						
							// menyimpan username dan level ke dalam session
							$_SESSION['level'] = $data['level'];
							$_SESSION['username'] = $data['username'];
							$_SESSION['id_staf'] = $data['id_staf'];
						
							// tampilkan menu
							header("location: staf/surat.php");						
						}
						else 
							echo 
									"<span style=color:red><b>Login Gagal, username atau password Salah!</b></p></span>";		
					}
			?>
			<!--End PHP untuk login-->
			
					<?php
					if (isset($_POST['batal'])) {
						echo "<script>window.close();</script>";
					}
				?>
		</div>
	</div>
	
</form>
</body>
</html>
<?php ob_flush(); ?>