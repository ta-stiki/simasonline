-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2017 at 03:08 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+08:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simasonl_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_disposisi`
--

CREATE TABLE IF NOT EXISTS `tbl_disposisi` (
  `no_surat` varchar(50) DEFAULT NULL,
  `id_staf` varchar(25) DEFAULT NULL,
  `status_terbaca` varchar(20) DEFAULT NULL,
  `tgl_terbaca` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_disposisi`
--

INSERT INTO `tbl_disposisi` (`no_surat`, `id_staf`, `status_terbaca`, `tgl_terbaca`) VALUES
('784/DPW.PPNI/S2/K.S/XI/2016', '19510904 197903 1 001', 'Tidak', NULL),
('2158/K7/2016', '16126', 'Tidak', NULL),
('005/21510/Set./BKBP', '19510904 197903 1 001', NULL, NULL),
('305/RSPM/DIR/XI/2016', '00035', NULL, NULL),
('4024/K8/KP/2016', '05060', NULL, NULL),
('Lorem.22.01.17', '1310', 'Tidak', NULL),
('Lorem.22.01.17', '13101498', 'Ya', '2017-01-21'),
('Lorem.22.01.17', '19510904 197903 1 001', 'Ya', '2017-01-21'),
('Ipsum.21.01.2017', '1310', 'Tidak', NULL),
('Ipsum.21.01.2017', '13101498', 'Tidak', NULL),
('Ipsum.21.01.2017', 'antara', 'Tidak', NULL),
('Ipsum.23.01.2017', '1234', 'Ya', '2017-02-03'),
('DL.02.02.01.TU.I.17', '1310', 'Ya', '2017-02-11'),
('DL.02.02.01.TU.I.17', '13101498', 'Ya', '2017-01-23'),
('DL.02.02.01.TU.I.17', '19510904 197903 1 001', 'Ya', '2017-01-29'),
('DL.02.02.02.TU.I.17', '1234', 'Ya', '2017-02-11'),
('DL.02.02.02.TU.I.17', '19510904 197903 1 001', 'Ya', '2017-01-29'),
('DL.02.02.03.TU.I.17', '19510904 197903 1 001', 'Ya', '2017-01-29'),
('DL.02.02.02.TU.II.17', '19510904 197903 1 001', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '00035', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '03053', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '05060', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '09075', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '09077', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '09078', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '09079', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '1310', 'Ya', '2017-02-11'),
('DL.02.02.01.TU.II.17', '131010', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '13101498', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '16126', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '19510904 197903 1 001', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', '83005', 'Tidak', NULL),
('DL.02.02.01.TU.II.17', 'antara', 'Tidak', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jabatan`
--

CREATE TABLE IF NOT EXISTS `tbl_jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jabatan`
--

INSERT INTO `tbl_jabatan` (`id_jabatan`, `jabatan`, `status`) VALUES
(3, 'Ketua', 'Aktif'),
(4, 'PUKET I', 'Aktif'),
(5, 'PUKET II', 'Aktif'),
(6, 'PUKET III', 'Aktif'),
(7, 'Kepala Prodi S1', 'Aktif'),
(8, 'Kepala Prodi DIII Bidan', 'Aktif'),
(9, 'Kepala Prodi  DIII Keperawatan', 'Aktif'),
(10, 'Dosen', 'Aktif'),
(11, 'Staf', 'Aktif'),
(13, 'Sekretaris Prodi S1 Perawat', 'Tidak Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prodi`
--

CREATE TABLE IF NOT EXISTS `tbl_prodi` (
  `id_prodi` int(11) NOT NULL,
  `prodi` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_prodi`
--

INSERT INTO `tbl_prodi` (`id_prodi`, `prodi`, `status`) VALUES
(1, 'S1 Ilmu Keperawatan', 'Aktif'),
(2, 'DIII Bidan', 'Aktif'),
(3, 'DIII Perawat', 'Aktif'),
(4, 'D IV Anastesi', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sifat_surat`
--

CREATE TABLE IF NOT EXISTS `tbl_sifat_surat` (
  `id_sifat` int(11) NOT NULL,
  `sifat_surat` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sifat_surat`
--

INSERT INTO `tbl_sifat_surat` (`id_sifat`, `sifat_surat`) VALUES
(1, 'Biasa'),
(3, 'Penting'),
(4, 'Rahasia'),
(2, 'Segera'),
(7, 'Tambah Sifat'),
(5, 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staf`
--

CREATE TABLE IF NOT EXISTS `tbl_staf` (
  `id_staf` varchar(25) NOT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_tlp` varchar(12) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_staf`
--

INSERT INTO `tbl_staf` (`id_staf`, `id_jabatan`, `id_prodi`, `nama`, `alamat`, `no_tlp`, `email`, `status`, `username`, `password`, `level`) VALUES
('00035', 4, 3, 'Luh Putu Dina Susanti,S.Kep,M.Kep', 'Jln. Pulau Panjang No. 3 ', '085935351328', 'nnlpdiinaa@yahooo.comm', 'Aktif', '00035', '00035', 'Staf'),
('03053', 5, 1, 'Ida Ayu Lysandari, S.E', 'Perum Candra Asri Biaung Blok E-10', '08123837860', '2idaayulysandari.stikesbali@gmail.com', 'Aktif', '03053', '03053', 'Staf'),
('05060', 10, 2, 'Ni Wayan Manik Parwati, S.Si.T.,M.Keb.', 'Pemogan, Denpasar Selatan', '081 2361 318', '2manik_parwati@yahooo.comm', 'Aktif', '05060', '05060', 'Staf'),
('09075', 11, 1, 'I Komang Agus Artha Wijaya, S.T', 'Dangin Puri Kaje', '097861608481', '2arthawijaya@gmail.com', 'Aktif', '09075', '09075', 'Admin'),
('09077', 4, 1, 'I Putu Gede Sutrisna.,Mpd', 'Karangasem', '085655656767', '2kadekgitayani@yahoo.com', 'Aktif', '09077', '09077', 'Admin'),
('09078', 5, 1, 'Ida Ayu Suptika,S.Kep', 'Bangli', '088765456788', '2kemarasujiani@gmail.com', 'Aktif', '09078', '09078', 'Staf'),
('09079', 4, 1, 'Ida Bagus Maha Gandamayu.,M.Kep', 'Karangasem', '086767677777', '2kadekbuja@gmail.com', 'Aktif', '09079', '09079', 'Staf'),
('1234', 10, 1, 'Ida Bagus Ary Indra Iswara.,M.Kom', 'Denpasar', '087861608481', 'aryindraiswara@gmail.com', 'Aktif', '1234', '1234', 'Staf'),
('1310', 11, 1, 'I Nyoman Widnyana Yasa', 'Jimbaran', '085738366395', 'widnyana1712@gmail.com', 'Aktif', '1310', '1310', 'Staf'),
('131010', 11, 1, 'Jaya-edit', 'antiga', '087861608481', 'want.jayasmartalways@gmail.com', 'Aktif', '131010', '131010', 'Staf'),
('13101498', 11, 4, 'I Putu Mahesa kama Artha', 'Batubulan', '08170670571', 'mahesakamaartha@gmail.com', 'Aktif', '13101498', '13101498', 'Staf'),
('16126', 10, 1, 'Ns. Anselmus Aristo Parut, S.Kep.,M.Kep.Trop', 'Jl. Tukad Yeh Aya, Denpasar-Renon', '082338807640', '2aarisparut22@gmail.comm', 'Aktif', '16126', '16126', 'Staf'),
('19510904 197903 1 001', 3, 1, 'I Ketut Widia,B.N.Stud.,M.M', 'Jln. Pulau Misol Gang V C DPS', '08179740401', 'want.jayasmartalways@gmail.com', 'Aktif', '19510', '19510', 'Ketua'),
('83005', 6, 1, 'Ns. AA A Yuliati Darmini,S.Kep., MNS', 'Jl. Tukad Pancoran no. 20 DPS', '081557768 00', '2yuliatidarmini@yahoo.com', 'Aktif', '83005', '83005', 'Staf'),
('antara', 11, 1, 'I Wayan Antara Jaya', 'Antiga-Karangasem', '087861608481', 'want.jayasmartalways@gmail.com', 'Aktif', 'antara', 'antara', 'Staf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat`
--

CREATE TABLE IF NOT EXISTS `tbl_surat` (
  `no_surat` varchar(50) NOT NULL,
  `id_sifat` int(11) DEFAULT NULL,
  `surat_dari` varchar(50) DEFAULT NULL,
  `prihal` varchar(50) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `tgl_surat` date DEFAULT NULL,
  `tgl_terima` date DEFAULT NULL,
  `konten` text,
  `keterangan` varchar(255) DEFAULT NULL,
  `jenis_surat` varchar(20) DEFAULT NULL,
  `lampiran` varchar(255) DEFAULT NULL,
  `approve` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_surat`
--

INSERT INTO `tbl_surat` (`no_surat`, `id_sifat`, `surat_dari`, `prihal`, `kepada`, `tgl_surat`, `tgl_terima`, `konten`, `keterangan`, `jenis_surat`, `lampiran`, `approve`) VALUES
('005/21510/Set./BKBP', 2, 'Pemerintah Provinsi Bali', 'Undangan Kegiatan Nusantara Bersatu', '', '2016-11-28', '2017-01-07', '', '', 'Masuk', '39609-Undangan Kegiatan.pdf', NULL),
('2158/K7/2016', 5, 'DIKTI', 'Rekomendasi Pindah Homebase', NULL, '2016-10-14', '2017-01-07', NULL, '', 'Masuk', '55660-Rekomendasi Pindah Homebase.pdf', NULL),
('305/RSPM/DIR/XI/2016', 5, 'Prima Medika Hospital', 'Permohonan Verifikasi Ijazah', '', '2016-11-25', '2017-01-07', '', '', 'Masuk', '5101-Permohonan Verifikasi Ijazah.pdf', NULL),
('4024/K8/KP/2016', 5, 'Kordinator Kopertis Wilayah VIII', 'Surat Keputusan Inpasing', '', '2016-08-08', '2017-01-07', '', '', 'Masuk', '61218-Keputusan Kordinator Kopertis.pdf', NULL),
('784/DPW.PPNI/S2/K.S/XI/2016', 5, 'PPNI', 'Penilaian Satuan Kredit Profesi', NULL, '2016-11-19', '2017-01-07', NULL, '', 'Masuk', '84629-PPNI.pdf', NULL),
('DL.02.02.01.TU.I.17', 1, 'TU', 'Undangan Rapat Dies Natalis', 'Yth. Putu Pratana Adi Wiharmika', '2017-01-24', '2017-01-23', '<p style="text-align: justify;"><span style="font-size: 12pt;">Dengan Hormat,</span></p>\r\n<p style="text-align: justify;"><br /><span style="font-size: 12pt;">Dalam rangka mempersiapkan rangkaian acara menyambut Dies Natalis STIKES Bali yang ke XI, kami mengharapkan kehadiran Bapak/Ibu/Saudara untuk menghadiri rapat koordinasi yang akan kami adakan pada :</span><br /><br /><span style="font-size: 12pt;">Hari/Tanggal&nbsp; : Sabtu, 9 April 2016</span><br /><span style="font-size: 12pt;">Pukul&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Pk. 08.00 Wita</span><br /><span style="font-size: 12pt;">Tempat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Ruang Makan Lt. IV Kampus II STIKES Bali</span><br /><span style="font-size: 12pt;">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jalan Tukad Balian No.180 Renon</span><br /><br /><span style="font-size: 12pt;">Demikianlah surat ini kami sampaikan, atas perhatian dan kerjasama yang baik kami mengucapkan terima kasih.</span></p>', '', 'Keluar', NULL, 'Ya'),
('DL.02.02.01.TU.II.17', 1, 'TU', 'Pelaksanaan Apel Paripurna', 'Yth. Seluruh Staf', '2017-02-05', '2017-02-05', '<p>Apel. Hari Senin Tgl. 6/2/17</p>', '', 'Keluar', NULL, 'Ya'),
('DL.02.02.02.TU.I.17', 3, 'TU', 'Pelaksanaan Apel Paripurna', 'Yth. Ketua Program Studi  D III Keperawatan dan Ilmu Keperawatan', '2017-01-07', '2017-01-23', '<p style="text-align: justify;">Dengan Hormat,<br /><br />Terkait dengan pelaksanaan apel perayaan Hari Kemerdekaan Republik Indonesia ke-71, bersama ini kami informasikan tentang pelaksanaan apel tersebut pada :<br /><br />Hari/Tanggal&nbsp; : Rabu, 17 Agustus 2016<br />Pukul&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Pk. 07.00 Wita &ndash; selesai (Upacara Pengibaran Bendera)<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pk. 16.00 Wita &ndash; selesai (Upacara Penurunan Bendera)&nbsp;&nbsp;&nbsp; <br />Tempat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Halaman Kampus Timur STIKES Bali<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jalan Tukad Balian No 180 Denpasar<br /><br />Demi kelancaran pelaksanaan apel paripurna tersebut diatas, kami mengharapkan agar saudara mempersiapkan segala sesuatu yang diperlukan untuk melaksanakan apel paripurna termasuk melaksanakan gladi minimal dua hari sebelumnya.<br /><br />Demikianlah surat ini kami sampaikan, agar dapat dilaksanakan dengan sebaik-baiknya, atas perhatian dan kerjasama yang baik kami mengucapkan terima kasih.</p>', '', 'Keluar', NULL, 'Ya'),
('DL.02.02.02.TU.II.17', 3, 'TU', 'Rapat Kordinasi ', 'Yth. Seleuh Dosen DIII Kebidanan', '2017-02-05', '2017-02-05', '<p>Selasa, 7-2-17</p>', NULL, 'Keluar', NULL, 'Tidak'),
('DL.02.02.03.TU.I.17', 1, 'TU', 'Rapat Kordinasi', 'Yth. Staf Dosen Perawat', '2017-01-23', '2017-01-26', '<p>Dengan Hormat,<br /><br />Terkait dengan pelaksanaan apel perayaan Hari Kemerdekaan Republik Indonesia ke-71 bersama ini kami informasikan tentang pelaksanaan apel tersebut pada :<br /><br />Hari/tanggal : Rabu, 17 Agustus 2016<br /><br />Pukul&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Pk.07.00 Wita - selesai (Upacara Pengibaran Bendera)<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pk.16.00 Wita - selesai (Upacara Penurunan Bendera)<br /><br />Tempat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Halaman Kampus Timur Stikes Bali<br /><br /><br />Demi kelancaran pelaksanaan apel paripurna tersebut diatas, kami mengharapkan agar saudara mempersiapkan segala sesuatu yang diperlukan untuk melaksanakan apel paripurna termasuk pelaksanaan gladi minimal dua hari sebelumnya.<br /><br />Demikianlah surat ini kami sampaikan, agar dapat dilaksanakan dengan sebaik-baiknya, atas perhatian dan kerjasamanya kami mengucapkan terimakasih</p>', '', 'Keluar', NULL, 'Ya'),
('Ipsum.21.01.2017', 2, 'Dikti', 'Pelatihan Feeder', NULL, '2017-01-21', '2017-01-23', NULL, '', 'Masuk', '67852-Lorem Ipsum Dolor Sit Amet-1.pdf', NULL),
('Ipsum.23.01.2017', 3, 'DIKTI', 'PELATIHAN FEEDER', NULL, '2017-01-23', '2017-01-23', NULL, '', 'Masuk', '43398-Lorem Ipsum Dolor Sit Amet.pdf', NULL),
('Lorem.22.01.17', 2, 'Contoh Surat Masuk 1', 'Pelatihan Feeder', NULL, '2017-01-22', '2017-01-21', NULL, '', 'Masuk', '31236-Lorem Ipsum Dolor Sit Amet-1.pdf', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_disposisi`
--
ALTER TABLE `tbl_disposisi`
  ADD KEY `no_surat` (`no_surat`), ADD KEY `id_staf` (`id_staf`);

--
-- Indexes for table `tbl_jabatan`
--
ALTER TABLE `tbl_jabatan`
  ADD PRIMARY KEY (`id_jabatan`), ADD UNIQUE KEY `jabatan` (`jabatan`);

--
-- Indexes for table `tbl_prodi`
--
ALTER TABLE `tbl_prodi`
  ADD PRIMARY KEY (`id_prodi`), ADD UNIQUE KEY `prodi` (`prodi`);

--
-- Indexes for table `tbl_sifat_surat`
--
ALTER TABLE `tbl_sifat_surat`
  ADD PRIMARY KEY (`id_sifat`), ADD UNIQUE KEY `sifat_surat` (`sifat_surat`);

--
-- Indexes for table `tbl_staf`
--
ALTER TABLE `tbl_staf`
  ADD PRIMARY KEY (`id_staf`), ADD UNIQUE KEY `password` (`password`), ADD UNIQUE KEY `username` (`username`), ADD KEY `id_jabatan` (`id_jabatan`), ADD KEY `id_prodi` (`id_prodi`);

--
-- Indexes for table `tbl_surat`
--
ALTER TABLE `tbl_surat`
  ADD PRIMARY KEY (`no_surat`), ADD KEY `id_sifat` (`id_sifat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_jabatan`
--
ALTER TABLE `tbl_jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_prodi`
--
ALTER TABLE `tbl_prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_sifat_surat`
--
ALTER TABLE `tbl_sifat_surat`
  MODIFY `id_sifat` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_disposisi`
--
ALTER TABLE `tbl_disposisi`
ADD CONSTRAINT `tbl_disposisi_ibfk_1` FOREIGN KEY (`no_surat`) REFERENCES `tbl_surat` (`no_surat`) ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_disposisi_ibfk_2` FOREIGN KEY (`id_staf`) REFERENCES `tbl_staf` (`id_staf`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_staf`
--
ALTER TABLE `tbl_staf`
ADD CONSTRAINT `tbl_staf_ibfk_3` FOREIGN KEY (`id_jabatan`) REFERENCES `tbl_jabatan` (`id_jabatan`) ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_staf_ibfk_4` FOREIGN KEY (`id_prodi`) REFERENCES `tbl_prodi` (`id_prodi`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_surat`
--
ALTER TABLE `tbl_surat`
ADD CONSTRAINT `tbl_surat_ibfk_1` FOREIGN KEY (`id_sifat`) REFERENCES `tbl_sifat_surat` (`id_sifat`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
