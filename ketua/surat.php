<?php ob_start();?>
<?php session_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['id_staf']) and ($_SESSION['level'] == "Ketua"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!--Select2 Plugin-->
				<link href="../select2-master/dist/css/select2.min.css" rel="stylesheet">
				
				<!-- Bootstrap core CSS -->
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				<script src="../select2-master/dist/js/select2.min.js"></script>	<!--Select2 Plugin-->
				<link href="../assets/css/custom-ketua.css" rel="stylesheet">
				<script type="text/javascript" src="../assets/tinymce/tinymce.min.js"></script>
				<!-- Datatabel Plugin -->
				<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
				<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
				<script src="../datatabel/js/jquery.dataTables.js"></script>
				<script src="../datatabel/js/fixedtabel.min.js"></script>
				<!--Datepicker Plugin-->
				<script src="../assets/js/bootstrap-datepicker.js"></script>
				<link href="../assets/css/datepicker.css" rel="stylesheet">				
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Data Surat</b></h4>
								</header>
							</div>
						</center>
						</br>
																
						<div id="batas" class="module_content">
							
								<div class="table-responsive">
									<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
										<thead>
											<tr bgcolor="#F5F5F5">
												<th>No </th>
												<th>Surat Dari &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>No Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Prihal &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Sifat Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Terima &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Nama Disposisi &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Jenis Surat &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status Terbaca &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Tgl. Terbaca  &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Status Approve &nbsp;&nbsp; &#8593;&#8595;</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
												include("../assets/koneksi/koneksi.php");									
											?>
											<?php
												$view=mysql_query("SELECT *
																		FROM tbl_surat INNER JOIN tbl_disposisi on (tbl_surat.no_surat=tbl_disposisi.no_surat)
																		inner join tbl_staf on (tbl_staf.id_staf=tbl_disposisi.id_staf)
																		inner join tbl_sifat_surat on (tbl_surat.id_sifat=tbl_sifat_surat.id_sifat)
																		where tbl_disposisi.id_staf='$_SESSION[id_staf]' and tbl_surat.approve='Ya' or (tbl_surat.lampiran != '' and tbl_disposisi.id_staf='$_SESSION[id_staf]')order by tbl_surat.jenis_surat ASC
																	");
												$no=0;
												while($row=mysql_fetch_array($view)){
												$no++;
											?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $row['surat_dari'];?></td>
													<td><?php echo $row['no_surat'];?></td>
													<td><?php echo $row['prihal'];?></td>
													<td><?php echo $row['sifat_surat'];?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_surat']));?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_terima']));?></td>
													<td><?php echo $row['nama'];?></td>
													<td><?php echo $row['jenis_surat'];?></td>
													<td><?php echo $row['status_terbaca'];?></td>
													<td><?php echo date("d-m-Y",strtotime ($row['tgl_terbaca']));?></td>
													<td><?php echo $row['approve'];?></td>
													<td> 
														<form method="POST" action="">
															<input type="hidden" name="no_surat" value="<?php echo $row['no_surat']; ?>">
															<input type="hidden" name="id_staf" value="<?php echo $row['id_staf']; ?>">
															<?php if ($row['lampiran']) { ?>
															<input type="submit" name="view5" value="View" class="btn btn-primary">
															<?php } ?>
															<?php if ($row['konten']) { ?>
															<input type="submit" name="view6" value="View" class="btn btn-danger">
															<?php } ?>
														</form>
														
														<!-- PHP Ubah Status Terbaca dan Tgl Terbaca-->
														<?php 
															if (isset($_POST['view5'])){
																$shows = mysql_query("SELECT * FROM tbl_surat INNER JOIN tbl_sifat_surat 
																ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)  WHERE no_surat='$_POST[no_surat]' ");
																while ($show = mysql_fetch_array($shows)) { 
																$image=$show['lampiran'] ;
																
																$terbaca=date('Y-m-d');
																$view5= mysql_query("UPDATE tbl_disposisi set status_terbaca='Ya',tgl_terbaca='$terbaca' where no_surat='$_POST[no_surat]' and id_staf='$_POST[id_staf]'");
																if ($view5){
																	echo "<script language='javascript'> document.location='../uploads/$image',target='_blank';</script>"; 
																}
															}
															}
														?>
														<!-- End PHP Ubah Status Terbaca dan Tgl Terbaca-->
														
														<!-- PHP Ubah Status Terbaca dan Tgl Terbaca-->
														<?php 
															if (isset($_POST['view6'])){
																$terbaca=date('Y-m-d');
																$view6= mysql_query("UPDATE tbl_disposisi set status_terbaca='Ya',tgl_terbaca='$terbaca' where no_surat='$_POST[no_surat]' and id_staf='$_POST[id_staf]' ");
																if ($view6){
																	echo "<script language='javascript'> document.location='../ketua/view_surat.php?no_surat=$_POST[no_surat]';</script>"; 
																	echo "<script language='javascript'> document.location='../ketua/view_surat.php?id_staf=$_POST[id_staf]';</script>"; 
																}
															}
														?>
														<!-- End PHP Ubah Status Terbaca dan Tgl Terbaca-->
														
													</td>
												</tr>
												<?php
												}
												?>
										</tbody>
										<!-- JavaScript Untuk datatabel scroll-->
										<script>
											$(document).ready(function() {
												$('#example').DataTable( {
													"scrollY": 200,
													"scrollX": true,
													fixedColumns:   {
													"leftColumns": 2,
													"rightColumns": 1
												}
												} );
											} );
										</script>
										<!-- end JavaScript Untuk datatabel scroll-->
									</table>
								</div>
						</div>
					</article>
				</div>
											
				<!-- Modal Edit Surat -->
				<?php if (isset($_POST['btnEdit'])) { ?>
					
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="" enctype="multipart/form-data" >
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Staf</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_surat INNER JOIN tbl_sifat_surat 
											ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)  WHERE no_surat='$_POST[no_surat]' ");
										  while ($show = mysql_fetch_array($shows)) { 
										  $image=$show['lampiran'] ;
										?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No_surat</label>
															<div class="col-sm-10">
																<input type="hidden" name="no_surat" value="<?php echo $show['no_surat']; ?>">
																<input name="no_surat" id="no_surat" placeholder="Nomor Surat" class="form-control" type="text" value="<?php echo $show['no_surat']; ?>" readonly="readonly" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Surat Dari</label>
														<div class="col-sm-10">
															<input name="surat_dari" id="dari" placeholder="Surat Dari" class="form-control" type="text" value="<?php echo $show['surat_dari']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput" >Tgl. Surat</label>
														<div class="col-sm-4">
															<input name="tgl_surat" type="text" placeholder="Tanggal Surat" class="form-control" id="date2" name="date2" value="<?php echo $show['tgl_surat']; ?>" required>
														</div>
														<label class="col-sm-2 control-label" for="textinput">Sifat Surat</label>
														<div class="col-sm-4">																
																<select name="sifat_surat" required>
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_sifat_surat";
																		echo "<option value='' selected>--Pilih Sifat Surat--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			$select = ($qtabel['id_sifat'] == $show['id_sifat'])? "selected":null;
																			echo '<option '.$select.' value="'.$qtabel['id_sifat'].'">'.$qtabel['sifat_surat'].'</option>';				
																		}
																	?>
																</select>																									
														</div>
													</div>
													<!-- Text input-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Prihal</label>
														<div class="col-sm-10">
															<input name="prihal" type="text" placeholder="Prihal" class="form-control"  type="text" value="<?php echo $show['prihal']; ?>" required>
														</div>
													</div>
													<!-- Text input-->
													<?php if ($show['kepada']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Kepada</label>
														<div class="col-sm-10">
															<input name="kepada" type="text" placeholder="Kepada" class="form-control"  type="text" value="<?php echo $show['kepada']; ?>" required>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['konten']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Isi Surat</label>
														<div class="col-sm-10">
															<textarea name="isi" class="isi" ><?php echo $show['konten']; ?></textarea>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<?php if ($show['keterangan']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Keterangan</label>
														<div class="col-sm-10">
															<textarea name="keterangan" placeholder="Keterangan" class="form-control" ><?php echo $show['keterangan']; ?></textarea>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													<!-- Text input-->
													<?php if ($show['lampiran']) { ?>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Pilih File</label>
														<div class="col-sm-10">
															<input type="file" name="img" id="img" class="form-control" value="<?php echo $image ?>" />
															<a href="../uploads/<?php echo $image ?>" target="_blank"><?php echo $image ?></a>
														</div>
													</div>
													<?php } ?>
													<!-- Text input-->
													
													<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Jenis Surat</label>
																<div class="col-sm-4">
																	<select name="jenis_surat" required>
																		<option></option>
																		<?php
																		if ($show['jenis_surat'] == 'Masuk') {
																			echo "<option value='Masuk' selected>Masuk</option>";
																			echo "<option value='Keluar'>Keluar</option>";
																		  }
																		  if ($show['jenis_surat'] == 'Keluar') {
																			echo "<option value='Keluar' selected>Keluar</option>";
																			echo "<option value='Masuk'>Masuk</option>";
																		  }
																		 ?>
																	  </select>				
																</div>
														</div>
													
													<!-- PHP Select Tangging/get-->						
													<?php
														$disposisiAwal = [];
														$shows2 = mysql_query("SELECT * FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]' ");
														while($show2=mysql_fetch_array($shows2)){
														$disposisiAwal[]= $show2['id_staf'];
													}
													?>														
													<!-- PHP Select Tangging/get-->		
													
													<!--Tabs Disposisi Surat-->
													<div class="form-group">
														<label class="col-sm-2 control-label" for="textinput">Disposisi Ke</label>
															<div class="col-sm-10">
																<select id="disposisi3" name="disposisi3[]" class="js-example-responsive" multiple="multiple" style="width: 100%" >
																	<option class="form-control" ></option>
																		<?php
																			$tampil=mysql_query("select * from tbl_staf order by id_staf asc");
																			while ($data = mysql_fetch_array($tampil))
																			{
																				foreach($disposisiAwal as $staff){
																					if ($data['id_staf']==$staff){
																						echo '<option class="form-control" value= "'.$data['id_staf'].'"  selected="selected">'.$data['nama'].'</option>';
																					}
																					else{
																						echo '<option class="form-control" value= "'.$data['id_staf'].'">'.$data['nama'].'</option>';
																					}
																				}
																			}
																		?>	
																</select>	
															</div>
															
															<!--java Scrip untuk select2-->
															<script>
																$(document).ready(function(){
																	$("#disposisi3").select2({
																		placeholder: "Please Select",
																		tags: true,
																		tokenSeparators: [','],
																	});
																})
																
															</script>
															<!--End java Scrip untuk select2-->
													</div>
													<!--Tabs Disposisi Surat-->
													</fieldset>
													<div class="modal-footer">
														<button name="btnUpdate" class="btn btn-primary" type="submit">Update</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
													<?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Surat -->						
				<?php } ?>				
				
				<!-- PHP Update Surat -->
				<?php
					error_reporting(0);
					if (isset($_POST['btnUpdate'])) {
						$tgl_terima=date('y-m-d');
	
						//<!-- PHP Simapan Images -->
						$ekstensi_diperbolehkan	= array('png','jpg','pdf');
						$folder="../uploads/";
						$img = rand(1000,100000)."-".$_FILES['img']['name'];
						$x = explode('.', $img);
						$ekstensi = strtolower(end($x));
						$img_loc = $_FILES['img']['tmp_name'];		
						if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
							move_uploaded_file($img_loc,$folder.$img);
				
						}
						else{
								echo 'EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
							}
						//<!-- PHP Simapan Images -->
						
						//PHP UPDATE data surat di tabel surat
						$insert_query = mysql_query("UPDATE tbl_surat SET id_sifat='$_POST[sifat_surat]', surat_dari='$_POST[surat_dari]'
																					, prihal='$_POST[prihal]', kepada='$_POST[kepada]'
																					, tgl_surat='$_POST[tgl_surat]', konten='$_POST[isi]'
																					, keterangan='$_POST[keterangan]', jenis_surat='$_POST[jenis_surat]'
																					,lampiran='$_POST[img]' 
																					WHERE no_surat='$_POST[no_surat]' ");
						if($insert_query) {
							$insert_query = mysql_query("DELETE FROM tbl_disposisi WHERE no_surat='$_POST[no_surat]'" 	); //menhapus telebih dahulu data disposisi
							$jml = count($_POST['disposisi3']);
							if(count($_POST['disposisi3'])){ 
								foreach($_POST['disposisi3'] as $staff){
									$insert_query = mysql_query("INSERT INTO tbl_disposisi (no_surat, id_staf) VALUES ('$_POST[no_surat]', '$staff') "); ///insert kembali data disposisi
								}
							}
						echo '<script language="javascript">alert("Data Surat Berhasil Diubah"); document.location="../admin/surat.php";</script>'; 
						} 
							else
								{
									echo '<script language="javascript">alert("Data Surat Gagal Diubah"); document.location="../admin/surat.php";</script>';  
								}
					}
				?>
				<!-- End PHP Update Surat -->
								
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				<!--JavaScript Untuk datatabel-->
				<script>
				$(document).ready(function() {
				$('#example').DataTable();
				} );
				</script>
				
				<script type="text/javascript">
				  // SHOW EDIT MODAL ON LOAD
					$(window).load(function(){
						$("#editModal").modal("show");
						$("#addModal").modal("show");
					});
				</script>
				<!-- End JavaScript Untuk datatabel-->
				
				<!-- Javascrip Datepicker-->
				<script>
					$(function () {
					$("#date2").datepicker({ 
							autoclose: true, 
							format: 'yyyy-mm-dd',
							autoclose: true,
					}).datepicker(new Date());;
					});
				</script>
				<!--End Javascrip Datepicker-->
				
				<!--Javascrip Tynimce-->
				<script type="text/javascript">
					tinymce.init({
					selector: ".isi",
					height : 400,
					// ===========================================
					// INCLUDE THE PLUGIN
					// ===========================================
					
					// ===========================================
					// PUT PLUGIN'S BUTTON on the toolbar
					// ===========================================
					
					toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | fontselect | fontsizeselect",
		
					// ===========================================
					// SET RELATIVE_URLS to FALSE (This is required for images to display properly)
					// ===========================================
						});
				</script>
				<!--Javascrip Tynimce-->
				
			</body>
		</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>