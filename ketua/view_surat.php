<?php ob_start();?>
<?php 	session_start();?>
<?php
	include("../assets/koneksi/koneksi.php");

?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['id_staf'])  and ($_SESSION['level'] == "Ketua"))
	{?>
<html>
	<head>
		<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
        <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
		<!--CSS Print Laporan -->
		<link href="../assets/css/print_laporan.css" rel="stylesheet" >
	</head>
	<body onload="print">
		<div id="main-wrapper">
			<?php if (isset ($_GET['no_surat'])) { 
				$surat=$_GET['no_surat'];	
					$shows = mysql_query("SELECT * FROM tbl_surat 
																INNER JOIN tbl_sifat_surat ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)
																INNER  JOIN tbl_disposisi ON (tbl_surat.no_surat=tbl_disposisi.no_surat)
																WHERE tbl_surat.no_surat='$surat' and tbl_disposisi.id_staf='$_SESSION[id_staf]' ");
					while ($dataTampil = mysql_fetch_array($shows)) {
				?>
                        <div class="kop-surat">
                            <table width="100%">
                                <tr>
                                    <td width="25" align="center"><img src="../assets/Images/IMG-20180805-WA0000a.png"></td>
                                    <td width="70" align="center" class="kop-surat">
                                        <h1>SMK NEGERI I BEBANDEM</h1>
                                        <p><b>IJIN NO: 094/2054/disdikpora</b></p>
                                        <p>Jl. Kuncaragiri, Sibetan, Bebandem, Kabupaten Karangasem, Bali 80861</p>
                                        <p>email:smknsatubebandem@yahoo.co.id</p>
                                    </td>
                                </tr>
                            </table>
                            <hr class="header-border">
                            </br>
                        </div>
				<form method="get" action="">
					<table>
						<tbody>
							<tr>
								<td style="">Nomor Surat </td><td>: <?php echo $_GET['no_surat']; ?></td>
								<td style="padding-left:0%">Denpasar, <?php echo date("d F Y", strtotime($dataTampil['tgl_surat'])); ?></td>
							</tr>
							<tr>
								<td style="">Sifat </td><td>: <?php echo $dataTampil['sifat_surat']; ?></td>
							</tr>
							<tr>
								<td style="">Prihal </td><td>: <?php echo $dataTampil['prihal']; ?></td>
							</tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr>
								<td>Kepada </td><td style="padding-right:30%">: <?php echo $dataTampil['kepada']; ?></td>
							</tr>
							<tr>
								<td></td><td>di- </td><td></td>
							</tr>
							<tr>
								<td></td><td>Tempat </td><td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<tbody>
							<tr><td><?php echo $dataTampil['konten']; ?><td></tr>
						</tbody>
					</table>
					<table style="width:100%;position:right;padding-left:60%">
						<tbody>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<?php 
								$shows2 = mysql_query("SELECT * FROM tbl_disposisi
																			INNER JOIN tbl_staf ON (tbl_staf.id_staf = tbl_disposisi.id_staf) 
																			INNER  JOIN tbl_surat ON (tbl_surat.no_surat=tbl_disposisi.no_surat)  
																			WHERE tbl_surat.no_surat='$surat' and tbl_disposisi.id_staf='$_SESSION[id_staf]'");
								while ($dataTampil2 = mysql_fetch_array($shows2)) { 
							?>
							<tr><td>Sekolah Tinggi Ilmu Kesehatan Bali</td></tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ketua,<td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<?php
								$shows3 = mysql_query("SELECT approve AS TTD FROM tbl_surat 
																INNER JOIN tbl_sifat_surat ON (tbl_surat.id_sifat = tbl_sifat_surat.id_sifat)
																INNER  JOIN tbl_disposisi ON (tbl_surat.no_surat=tbl_disposisi.no_surat)
																WHERE tbl_surat.no_surat='$surat' and tbl_disposisi.id_staf='$_SESSION[id_staf]'");
								while ($dataTampil4 = mysql_fetch_array($shows3)) { 

								if (($dataTampil4['TTD']) =='Ya'){
							
							?>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									<?php echo 'TTD Approved By :' ;?>
								</td>
							</tr>
							<?php }
							
								if (($dataTampil4['TTD']) =='Tidak'){
							
							?>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									<?php echo 'Not Approved' ;?>
								</td>
							</tr>
							<?php } 
								
								}?>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td><u><?php echo $dataTampil2['nama'];?></u><td></tr>
							<tr><td>NIP. <?php echo $dataTampil2['id_staf'];?><td></tr>
							<?php } ?>
						</tbody>
					</table>
					<table>
						<tbody>
							<tr><td  style="padding-bottom:40%"></td></tr>
							<tr><td>Tembusan disampaikan Kepada Yth.<td></tr>
							<tr><td>1. Ketua YP3LPK Bali di Denpasar<td></tr>
							<tr><td>2. Arsip<td></tr>
						</tbody>
					</table>
						<?php } ?>
				</form>
			<?php } ?>
		</div>
	</body>
</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>
