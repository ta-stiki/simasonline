<?php session_start();?>
<?php ob_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
	error_reporting(0);
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['level'] == "Ketua"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!-- Bootstrap core CSS -->
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				<link href="../assets/css/custom.css" rel="stylesheet">
				<script src="../assets/js/highcharts.js"></script>
				<script src="../assets/js/exporting.js"></script>
				<!-- Datatabel Plugin -->
				<link href="../datatabel/css/jquery.dataTables.css" rel="stylesheet" media="screen">
				<link href="../datatabel/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
				<script src="../datatabel/js/jquery.dataTables.js"></script>
				<script src="../datatabel/js/fixedtabel.min.js"></script>
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Laporan Surat Masuk</b></h4>
								</header>
							</div>
						</center>
						</br>
						<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
							<!-- Text input Load Bulan dari database-->
							<div class="form-group">
								<div class="col-sm-1">																
									<select name="bulan">
										<option value="">-Pilih Bulan-</option>
										<option value="01">Januari</option>
										<option value="02">Pebruari</option>
										<option value="03">Maret</option>
										<option value="04">April</option>
										<option value="05">Mei</option>
										<option value="06">Juni</option>
										<option value="07">Juli</option>
										<option value="08">Agustus</option>
										<option value="09">September</option>
										<option value="10">Oktober</option>
										<option value="11">Nopember</option>
										<option value="12">Desember</option>
									</select>																									
								</div>
								<!-- End Text input Load Bulan dari database-->
								
								<!-- Text input Load Tahun dari database-->
								<div class="col-sm-1">																
									<select name="tahun">
										<?php
											include("../assets/koneksi/koneksi.php");
											$query ="SELECT YEAR(tgl_surat) AS tahun, COUNT(*) AS jumlah_tahunan
													FROM tbl_surat
													WHERE jenis_surat='Masuk' GROUP BY YEAR(tgl_surat);";
											echo "<option value='' selected>-Pilih Tahun-</option>";
											$hasil = mysql_query($query);
											while ($qtabel = mysql_fetch_assoc($hasil))
											{
												echo '<option value="'.($qtabel['tahun']).'">'.($qtabel['tahun']).'</option>';				
											}
										?>
									</select>																									
								</div>
								<!-- ENd Text input Load Tahun dari database-->
								
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button name="tampil" class="btn btn-primary" type="submit">Tampilkan</button>
								<!--Query unutk melempar data bulan dan tahun jika buton tampil di pilih-->
								<?php if (isset($_POST['tampil'])) {
									// $tahun_sekarang = date('Y');
									// $bulan_sekarang = date('m'); ?>
									<a href="print1.php?cetak=1&bulan=<?php echo $_POST[bulan]; ?>&tahun=<?php echo $_POST[tahun]; ?>" target="_blank"><button name="cetak" class="btn btn-success" type="button">Cetak</button></a>
								<?php } else { ?>
									<a href="print1.php?cetak=1&bulan=<?php echo date('m'); ?>&tahun=<?php echo date('Y'); ?>" target="_blank"><button name="cetak" class="btn btn-success" type="button">Cetak</button></a>
								<?php } ?>
								<!--And Query unutk melempar data bulan dan tahun jika buton tampil di pilih-->
							</div>
						</form>
						
						
						<!--Untuk Merelod Tampilan Berdasarkan Bulan dan Tahun-->
						<?php if (isset($_POST['tampil'])){
						$bulan=$_REQUEST['bulan'];
						$tahun=$_REQUEST['tahun'];
							
						$saatini= date("Y-m", strtotime ($tahun.'-'.$bulan));
						if ((empty($bulan)) or (empty($tahun))) { ?>
							<div style="text-align: center;font-weight:bold;font-zise:18px">Filter Bulan dan Tahun Masih Kosong</div>
							<div style="text-align: center;font-weight:bold;font-zise:18px">Silakan Pilih Bulan dan Tahun Terlebih Dahulu</div>
						<?php }else{
					

							//Membuat Query
							$q=mysql_query("SELECT sifat_surat AS Sifat,surat_dari,jenis_surat, COUNT(*) AS jumlah
										FROM tbl_surat INNER JOIN tbl_sifat_surat ON tbl_surat.id_sifat=tbl_sifat_surat.id_sifat 
										WHERE jenis_surat='Masuk' and tgl_surat LIKE '%$saatini%' AND tgl_surat LIKE '%$tahun%' GROUP BY sifat_surat
										");									
						?>
						
						<!--LOAD Grafik-->
						<script type="text/javascript">
							$(function () {
								$('#view').highcharts({
									chart: {
										type: 'column'
									},
									title: {
										text: 'Jumlah Surat Masuk Berdasarkan Sifat Surat'
									},
									subtitle: {
										text: ''
									},
									xAxis: {
										categories: [
											'Sifat Surat'
										]
									},
									yAxis: {
										min: 0,
										title: {
											text: 'Total'
										}
									},
									plotOptions: {
											 series: {
												borderWidth: 0,
												dataLabels: {
													enabled: true,
													format: '{point.y}'
												}
											 }
										},
									series: [
									
									<?php
									while($r=mysql_fetch_array($q)){
										echo "{ name: '".$r[Sifat]."',data: [".$r[jumlah]."]},";
									}
									?>
									]
								});
							});
						</script>
							<div id="view" style="min-width: 310px; height: 400px; margin: 0 auto"></div>  	

							<!--Menampilkan Data Tabel Laporan-->
							<div id="batas" class="module_content">
									<div class="table-responsive">
										<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
											<br></br>
											<hr></hr>
											<hr></hr>
											<br></br>
											<thead>
												<tr bgcolor="#F5F5F5">
													<th>No </th>
													<th>Surat Dari &nbsp;&nbsp; &#8593;&#8595;</th>
													<th>No Surat &nbsp;&nbsp; &#8593;&#8595;</th>
													<th>Prihal &nbsp;&nbsp; &#8593;&#8595;</th>
													<th>Sifat Surat &nbsp;&nbsp; &#8593;&#8595;</th>
													<th>Tgl. Surat &nbsp;&nbsp; &#8593;&#8595;</th>
													<th>Tgl. Terima &nbsp;&nbsp; &#8593;&#8595;</th>
												</tr>
											</thead>
											<tbody>
											<?php
											include("../assets/koneksi/koneksi.php");									
											?>
												<?php
												$view=mysql_query("SELECT *
																		FROM tbl_surat
																		inner join tbl_sifat_surat on tbl_surat.id_sifat=tbl_sifat_surat.id_sifat
																		WHERE jenis_surat='Masuk' and tgl_surat LIKE '%$saatini%' and tgl_surat LIKE '%$tahun%'
																	");
												$no=0;
												while($row=mysql_fetch_array($view)){
													$no++;
												?>
													<tr>
														<td><?php echo $no;?></td>
														<td><?php echo $row['surat_dari'];?></td>
														<td><?php echo $row['no_surat'];?></td>
														<td><?php echo $row['prihal'];?></td>
														<td><?php echo $row['sifat_surat'];?></td>
														<td><?php echo date("d-m-Y",strtotime ($row['tgl_surat']));?></td>
														<td><?php echo date("d-m-Y",strtotime ($row['tgl_terima']));?></td>

													</tr>
												<?php
												}
												?>
											</tbody>
											<!-- JavaScript Untuk datatabel scroll-->
											<script>
												$(document).ready(function() {
													$('#example').DataTable( {
														"scrollY": 200,
														"scrollX": true
													}
													} );
												} );
											</script>
											<!-- end JavaScript Untuk datatabel scroll-->
										</table>
									</div>
							</div>
							<!--END Menampilkan Data Tabel Laporan-->
							
						<?php }
								}
						//<!--END LOAD Grafik-->
						
						//<!--Menampilkan Data Pada saat Pertama Akses-->
						else{
							$tahun_sekarang = date('Y');
							$bulan_sekarang = date('m');
								//Membuat Query
								$q=mysql_query("SELECT sifat_surat AS Sifat,surat_dari,jenis_surat, COUNT(*) AS jumlah
											FROM tbl_surat INNER JOIN tbl_sifat_surat ON tbl_surat.id_sifat=tbl_sifat_surat.id_sifat 
											WHERE jenis_surat='Masuk' and tgl_surat LIKE '%$bulan_sekarang%' AND tgl_surat LIKE '%$tahun_sekarang%' GROUP BY sifat_surat
											");									
							?>
							
							<!--Menampilkan Data Grafik-->		
							<script type="text/javascript">
								$(function () {
									$('#view').highcharts({
										chart: {
											type: 'column'
										},
										title: {
											text: 'Jumlah Surat Masuk Berdasarkan Sifat Surat'
										},
										subtitle: {
											text: ''
										},
										xAxis: {
											categories: [
												'Sifat Surat'
											]
										},
										yAxis: {
											min: 0,
											title: {
												text: 'Total'
											}
										},
										plotOptions: {
											 series: {
												borderWidth: 0,
												dataLabels: {
													enabled: true,
													format: '{point.y}'
												}
											 }
										},
										series: [
										
										<?php
										while($r=mysql_fetch_array($q)){
											echo "{ name: '".$r[Sifat]."',data: [".$r[jumlah]."]},";
										}
										?>
										]
									});
								});
							</script>
								<div id="view" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
								
								
								<!--Menampilkan Data Tabel Laporan-->
								<div id="batas" class="module_content">
										<div class="table-responsive">
											<table id="example" class="display nowrap table table-striped table-bordered table-hover table-condensed">
												<br></br>
												<hr></hr>
												<hr></hr>
												<br></br>
												<thead>
													<tr bgcolor="#F5F5F5">
														<th>No </th>
														<th>Surat Dari &nbsp;&nbsp; &#8593;&#8595;</th>
														<th>No Surat &nbsp;&nbsp; &#8593;&#8595;</th>
														<th>Prihal &nbsp;&nbsp; &#8593;&#8595;</th>
														<th>Sifat Surat &nbsp;&nbsp; &#8593;&#8595;</th>
														<th>Tgl. Surat &nbsp;&nbsp; &#8593;&#8595;</th>
														<th>Tgl. Terima &nbsp;&nbsp; &#8593;&#8595;</th>

													</tr>
												</thead>
												<tbody>
												<?php
												include("../assets/koneksi/koneksi.php");									
												?>
													<?php
													$view=mysql_query("SELECT *
																			FROM tbl_surat 
																			inner join tbl_sifat_surat on tbl_surat.id_sifat=tbl_sifat_surat.id_sifat
																			WHERE jenis_surat='Masuk' and tgl_surat LIKE '%$bulan_sekarang%' AND tgl_surat LIKE '%$tahun_sekarang%'
																		");
													$no=0;
													while($row=mysql_fetch_array($view)){
														$no++;
													?>
														<tr>
															<td><?php echo $no;?></td>
															<td><?php echo $row['surat_dari'];?></td>
															<td><?php echo $row['no_surat'];?></td>
															<td><?php echo $row['prihal'];?></td>
															<td><?php echo $row['sifat_surat'];?></td>
															<td><?php echo date("d-m-Y",strtotime ($row['tgl_surat']));?></td>
															<td><?php echo date("d-m-Y",strtotime ($row['tgl_terima']));?></td>

														</tr>
													<?php
													}
													?>
												</tbody>
												<!-- JavaScript Untuk datatabel scroll-->
												<script>
													$(document).ready(function() {
														$('#example').DataTable( {
															"scrollY": 200,
															"scrollX": true
														}
														} );
													} );
												</script>
												<!-- end JavaScript Untuk datatabel scroll-->
											</table>
										</div>
								</div>
								<!--END Menampilkan Data Tabel Laporan-->
							
						<?php
						}
						?>
						<!--End Menampilkan Data Pada saat Pertama Akses-->
						



					</article>
				</div>
								
				<!-- Modal Edit Staf -->
				<?php if (isset($_POST['btnEdit'])) { ?>
						<div class="modal fade bs-example-modal-lg" id="editModal" role="dialog">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<form method="POST" action="">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myLargeModalLabel3">Form Ubah Data Staf</h4>
									</div>
									<div class="modal-body">
										<?php
										  $shows = mysql_query("SELECT * FROM tbl_staf WHERE id_staf='$_POST[id_staf]' ");
										  while ($show = mysql_fetch_array($shows)) { 
											?>
										<div class="row">
											<div class="col-md-4 col-md-offset-4">
													<fieldset>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Id Staf</label>
															<div class="col-sm-10">
																<input type="hidden" name="id" value="<?php echo $show['id_staf']; ?>">
																<input name="id_staf" id="id_staf" placeholder="Id Staf" class="form-control" type="text" value="<?php echo $show['id_staf']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Nama</label>
															<div class="col-sm-10">
																<input name="nama" id="nama" placeholder="Input Nama Staf" class="form-control" type="text" value="<?php echo $show['nama']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Jabatan</label>
															<div class="col-sm-4">
																<select name="jabatan">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_jabatan";
																		echo "<option value='' selected>--Pilih Jabatan--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			echo '<option value="'.$qtabel['id_jabatan'].'">'.$qtabel['jabatan'].'</option>';				
																		}
																	?>
																</select>
															</div>
															<label class="col-sm-2 control-label" for="textinput">Prodi</label>
															<div class="col-sm-4">
																<select name="prodi">
																	<?php
																		include("../assets/koneksi/koneksi.php");
																		$query = "select * from tbl_prodi";
																		echo "<option value='' selected>--Pilih Prodi--</option>";
																		$hasil = mysql_query($query);
																		while ($qtabel = mysql_fetch_assoc($hasil))
																		{
																			echo '<option value="'.$qtabel['id_prodi'].'">'.$qtabel['prodi'].'</option>';				
																		}
																	?>
																</select>
															</div>
														</div>
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Alamat</label>
															<div class="col-sm-10">
																<input type="text" name="alamat" placeholder="Alamat" class="form-control"  type="text" value="<?php echo $show['alamat']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">No Telp</label>
															<div class="col-sm-10">
																<input type="text" name="telepon" placeholder="No Telp" class="form-control"  type="text" value="<?php echo $show['no_tlp']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">E-Mail</label>
															<div class="col-sm-10">
																<input type="email" name="email" placeholder="E-mail" class="form-control"  type="text" value="<?php echo $show['email']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Username</label>
															<div class="col-sm-10">
																<input type="text" name="username" placeholder="Username" class="form-control"  type="text" value="<?php echo $show['username']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Password</label>
															<div class="col-sm-10">
																<input type="text" name="password" placeholder="Password" class="form-control"  type="text" value="<?php echo $show['password']; ?>" required>
															</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Level</label>
																<div class="col-sm-4">
																	<select name="level" required>
																		<option></option>
																		<?php
																		if ($show['level'] == 'Ketua') {
																			echo "<option value='Admin'>Admin</option>";
																			echo "<option value='Ketua' selected>Ketua</option>";
																			echo "<option value='Staf'>Staf</option>";
																		  }
																		  if ($show['level'] == 'Admin') {
																			echo "<option value='Admin' selected>Admin</option>";
																			echo "<option value='Ketua'>Ketua</option>";
																			echo "<option value='Staf'>Staf</option>";
																		  }
																		  
																		  if ($show['level'] == 'Staf') {
																			echo "<option value='Admin'>Admin</option>";
																			echo "<option value='Ketua'>Ketua</option>";
																			echo "<option value='Staf' selected>Staf</option>";
																		  }
																		 ?>
																	  </select>																
																</div>
														</div>
														<!-- Text input-->
														<!-- Text input-->
														<div class="form-group">
															<label class="col-sm-2 control-label" for="textinput">Status</label>
																<div class="col-sm-4">
																	<select name="status" required>
																		<option></option>
																		<?php
																		if ($show['status'] == 'Aktif') {
																			echo "<option value='Aktif' selected>Aktif</option>";
																			echo "<option value='Tidak Aktif'>Tidak Aktif</option>";
																		  }
																		  if ($show['status'] == 'Tidak Aktif') {
																			echo "<option value='Tidak Aktif' selected>Tidak Aktif</option>";
																			echo "<option value='Aktif'>Aktif</option>";
																		  }
																		 ?>
																	  </select>				
																</div>
														</div>
														<!-- Text input-->
													</fieldset>
													<div class="modal-footer">
														<button name="btnUpdate" class="btn btn-primary" type="submit">Simpan</button>
														<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>												
													</div>
													<?php } ?>											
											</div>
											<!-- /.col-lg-12 -->
										</div>
										<!-- /.row -->
									</div>
									</form>
								</div>
							</div>
						</div>
						<!-- And Modal Edit Staf -->
				<?php } ?>				
				
				<!-- PHP Untuk Insert Data Staf -->
				<?php
					if (isset($_POST['btnAdd'])) {
						  $insert_query = mysql_query("INSERT INTO tbl_staf VALUES ('$_POST[id_staf]','$_POST[jabatan]','$_POST[prodi]','$_POST[nama]','$_POST[alamat]','$_POST[telepon]','$_POST[email]','Aktif','$_POST[username]','$_POST[password]','$_POST[level]')");
						  if($insert_query) {
							echo '<script language="javascript">alert("Data Staf Berhasil Disimpan"); document.location="../admin/staf.php";</script>'; 
						  } 
							else
								{
									echo '<script language="javascript">alert("Data Staf Gagal Disimpan"); document.location="../admin/staf.php";</script>';  
								}
					}
				?>
				<!-- And PHP Untuk Insert Data Staf  -->
				
				<!-- PHP Untuk Update Data Staf  -->
				<?php
					if (isset($_POST['btnUpdate'])) {
						  $update_query = mysql_query("UPDATE tbl_staf SET nama='$_POST[nama]', alamat='$_POST[alamat]', no_tlp='$_POST[telepon]', email='$_POST[email]', username='$_POST[username]', password='$_POST[password]', level='$_POST[level]', status='$_POST[status]' WHERE id_staf='$_POST[id]' ");
						  if($update_query) {
							echo "<script type='text/javascript'>alert('Data Staf Berhasil Diubah')</script>";
							echo "<script>setTimeout(\"location.href = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]';\",0);</script>";
						  } 
						  else
							{
								echo '<script language="javascript">alert("Data Staf Gagal Diubah"); document.location="../admin/staf.php";</script>';  
							}
					}
				?>
				<!-- And PHP Untuk Update Data Staf  -->
								
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				<!--JavaScript Untuk datatabel-->
				<script>
				$(document).ready(function() {
				$('#example').DataTable();
				} );
				</script>
				
				<script type="text/javascript">
				  // SHOW EDIT MODAL ON LOAD
					$(window).load(function(){
						$("#editModal").modal("show");
						$("#addModal").modal("show");
					});
				</script>
				<!-- End JavaScript Untuk datatabel-->
				
			</body>
		</html>
<?php
	}else{
		header("location: ../index.php");
	}
?>