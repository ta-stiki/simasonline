<?php ob_start();?>
<?php session_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['id_staf']) and ($_SESSION['level'] == "Ketua"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
				<link rel="icon" href="../assets/Images/Stikes.png" type="image/x-icon">
				<!-- Bootstrap core CSS -->
				
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<link href="../assets/css/custom-ketua.css" rel="stylesheet">
				<link href="../select2-master/dist/css/select2.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../select2-master/dist/js/select2.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Beranda</b></h4>
								</header>
							</div>
						</center>
						<div id="batas" class="module_content">
							</br>
							</br>
							</br>
							<center><b>Selamat Datang di SIMAS</b></center>
							<center><b>(Sistem Informasi Manajemen Surat)</b></center>
							<?php 
								$tahun = getdate();
							?>
							<center><b>Tahun Periode <?php echo $tahun["year"]; ?></b></center>
						</div>
					</article>
				</div>
				<!-----------And Content------------------------------------------------------------------------------------------------------->
				<br></br>
				<div id = "bataskiri">
				
				</div>
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				
				

			</body>
		</html>
<?php
	}else{
		header("location:logout.php");
	}
?>