<?php 	session_start();?>
<?php ob_start();?>
<!DOCTYPE html>
<?php
	include("../assets/koneksi/koneksi.php");
?>
<?php 
	if (isset($_SESSION['username']) and ($_SESSION['id_staf']) and ($_SESSION['level'] == "Staf"))
	{?>
		<html lang="en">
			<head>
				<title>SIMAS (Sistem Informasi Manajemen Surat)</title>
                <link rel="icon" type="image/png" sizes="192x192"  href="../assets/Images/pavicon/android-icon-192x192.png">
                <link rel="icon" type="image/png" sizes="32x32" href="../assets/Images/pavicon/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="96x96" href="../assets/Images/pavicon/favicon-96x96.png">
                <link rel="icon" type="image/png" sizes="16x16" href="../assets/Images/pavicon/favicon-16x16.png">
				<!-- Bootstrap core CSS -->
				
				<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
				<link href="../assets/css/custom-staf.css" rel="stylesheet">
				<link href="../select2-master/dist/css/select2.min.css" rel="stylesheet">
				<script src="../assets/js/jquery.min.js"></script>
				<script src="../select2-master/dist/js/select2.min.js"></script>
				<script src="../assets/js/bootstrap.min.js"></script>
				
			</head>
			<body>
			
				<!-- Start Header -->
					<?php
						include'header.php';
					?>
				<!-- And Header -->
				
				<!-- Static navbar -->
					<?php
						include'navbar-log.php';
					?>
				<!-- And Static navbar -->
				
				<!-----------Content------------------------------------------------------------------------------------------------------->
				<div id = "bataskiri">
					<article class="module width_full">
						<center>
							<div class="title">
								<header>
									<h4><b>Beranda</b></h4>
								</header>
							</div>
						</center>
						<div id="batas" class="module_content">
							</br>
							</br>
							</br>
							<center><b>Selamat Datang di SIMAS</b></center>
							<center><b>(Sistem Informasi Manajemen Surat)</b></center></b></i>
							<?php 
								$tahun = getdate();
							?>
							<center><b>Tahun Periode <?php echo $tahun["year"]; ?></b></center>
						</div>
					</article>
				</div>
				<!-----------And Content------------------------------------------------------------------------------------------------------->
				<br></br>
				<div id = "bataskiri">
				
				</div>
				<!-- Start Footer -->
				<?php 
					include'footer.php';
				?>
				<!-- Ended Footer -->
				<!-- Bootstrap core JavaScript
					================================================== -->
				<!-- Placed at the end of the document so the pages load faster -->
				
				
				

			</body>
		</html>
<?php
	}else{
		header("location:logout.php");
	}
?>